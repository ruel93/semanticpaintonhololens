function Plot_mesh_from_positions( A )
%Draws triangles assuming the matrix A is [x1, y1, z1; x2, y2, z2;...]

close all
height = size(A,1);

X = A(:,1);
Y = A(:,2);
Z = A(:,3);
%plot first line

plot3(X(1:2), Y(1:2), Z(1:2), '-or');
hold on

x1 = X(1); x2 = X(2);
y1 = Y(1); y2 = Y(2);
z1 = Z(1); z2 = Z(2);

for i = 3:height
   x3 = X(i); y3 = Y(i); z3 = Z(i);
   
   plot3([x1 x3], [y1 y3], [z1 z3], '-or');
   
   plot3([x2 x3], [y2 y3], [z2 z3], '-or');
   drawnow
   
   x1 = x2; x2 = x3;
   y1 = y2; y2 = y3;
   z1 = z2; z2 = z3;
end
end

