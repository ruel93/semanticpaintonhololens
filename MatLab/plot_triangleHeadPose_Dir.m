function intersection = plot_triangleHeadPose_Dir( A, B, C, HeadPos, HeadDir )
hold on;
plot_triangles_from_positions([A;B;C]);
drawnow

A = A'; B = B'; C = C'; HeadPos = HeadPos'; HeadDir = HeadDir';
normal = cross(B-A, C-A);
normal = normal/norm(normal)%Normal of the triangle
HeadDir = HeadDir/norm(HeadDir)
center = (A + B + C) / 3;
figure (1)
centerHead = plot3([HeadPos(1); center(1)], [HeadPos(2); center(2)], [HeadPos(3); center(3)], '-or');
legend(centerHead, 'Center-Head');
drawnow;


xt = @(t) center(1) + normal(1) * t;
yt = @(t) center(2) + normal(2) * t;
zt = @(t) center(3) + normal(3) * t;
normalPlot = fplot3(xt,yt,zt, [0, 0.1], 'g');
legend([centerHead normalPlot], 'CenterHead', 'Normal');
drawnow;

xt = @(t) HeadPos(1) + HeadDir(1) * t;
yt = @(t) HeadPos(2) + HeadDir(2) * t;
zt = @(t) HeadPos(3) + HeadDir(3) * t;
fplot3(xt,yt,zt, [-0.05, 0.05])
drawnow

Distance = dot(-normal,  HeadPos - center) / dot(normal, HeadDir)

intersection = HeadPos + Distance * HeadDir;

plot3(intersection(1), intersection(2), intersection(3), 'xb');

P = intersection;
AP = P - A;
BP = P - B;
AB = B - A;
AC = C - A;
BC = C - B;

checksign = @(x, y, z) dot(cross(x, y)/norm(cross(x, y)), cross(y,z)/norm(cross(y, z)));

test1 = checksign(AC, AP, AB)
test2 = checksign(BC, BP, -AB)


end