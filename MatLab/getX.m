
function X = getX(x)
global HeadPos HeadDir;
X = HeadPos(1) + HeadDir(1) * x;
end

function Y = getY(y)
global HeadPos HeadDir;
Y = HeadPos(2) + HeadDir(2) * y;
end

function Z = getZ(z)
global HeadPos HeadDir;
Z = HeadPos(3) + HeadDir(3) * z;
end