function plot_triangles_from_positions( A )
%Draws triangles assuming the matrix A is [x1, y1, z1; x2, y2, z2;...]
close all
%A = gpuArray(B);
height = size(A,1);
% X = A(:,1);
% Y = A(:,2);
% Z = A(:,3);
for ii = 1:3:height-2
    p1 = A(ii,:);
    p2 = A(ii+1,:);
    p3 = A(ii+2,:);
    X = [p1(1) p2(1) p3(1)];
    Y = [p1(2) p2(2) p3(2)];
    Z = [p1(3) p2(3) p3(3)];
%     if ((max(X) > 200) || (min(X) < -200))
%         break
%     end
%     if ((max(Y) > 200) || (min(Y) < -200))
%         break
%     end
%     if ((max(Z) > 200) || (min(Z) < -200))
%         break
%     end
    fill3(X, Y, Z, 0);

    hold on
end
    
%     plot3(X(ii:ii+1),Y(ii:ii+1),Z(ii:ii+1),'-or');
%     drawnow
%     pause(1);
%     hold onA
%     plot3(X(ii:ii+2),Y(ii:ii+2),Z(ii:ii+2),'-or');
%     drawnow
%     pause(1);
%     plot3(X(ii+1:ii+2),Y(ii+1:ii+2),Z(ii+1:ii+2),'-or');
%     drawnow
%     pause(3);
    %plot3(A(ii+1,1),A(ii+1,2),A(ii+1,3),A(ii+2,1),A(ii+2,2),A(ii+2,3),'-or');    
%     drawnow
%     pause (0.5);
%     plot3(A(ii+2,1),A(ii+2,2),A(ii+2,3),A(ii,1),A(ii,2),A(ii,3),'-or');    
%     drawnow
%     pause
end

