# README #

This README documents the requirements to compile and run the HoloLens version of SemanticPaint.

### Pre-requisites ###

* Visual Studio 2015 Update 3 or above (With Windows 10 SDK)
* Window 8 or above
* An HoloLens or an Emulator installed
* 32bit version of Boost library

### Emulator Setup ###

* Uncomment the line #define EMULATOR in SpaintTriangleMesh.h
* If you don't have a microphone, please uncomment the line #define NO_VOICE in SemanticPaintOnHoloLensMain.cpp

### HoloLens Setup ###

* The current commit is working on the HoloLens without any modifies. If you changed something in the step above, simply revert the changes and recompile


**For any questions please write me at matteo.ruello@gmail.com**