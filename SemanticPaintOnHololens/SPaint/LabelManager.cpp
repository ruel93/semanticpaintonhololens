/**
 * spaint: LabelManager.cpp
 * Copyright (c) Torr Vision Group, University of Oxford, 2015. All rights reserved.
 */

#include "pch.h"
#include "SPaint/LabelManager.h"

#include <algorithm>


#include <tvgutil/MapUtil.h>
using namespace tvgutil;

namespace {

	//#################### LOCAL CONSTANTS ####################
	typedef std::array<uint8, 3> Vector3u;
	/**
	 * Kelly's colours of maximum contrast (see https://eleanormaclure.files.wordpress.com/2011/03/colour-coding.pdf), black is used for the background label (it will be depicted as transparent
	 */
	const std::vector<Vector3u> colours = {
		{ 255, 255, 255 },
		{ 255, 179, 0 },
		{ 128, 62, 117},
		{ 255, 104, 0},
		{ 193, 0, 32},
		{ 206, 162, 98},
		{ 129, 112, 102},
		//// The remaining colours aren't good for people with defective colour vision:
		{ 0, 125, 52 },
		{ 246, 118, 142 },
		{ 0, 83, 138 },
		{ 255, 122, 92 },
		{ 83, 55, 122 },
		{ 255, 142, 0 },
		{ 179, 40, 81 },
		{ 244, 200, 0 },
		{ 127, 24, 13 },
		{ 147, 170, 0 },
		{ 89, 51, 21 },
		{ 241, 58, 19 },
		{ 35, 44, 22 }
	};



}

namespace SemanticPaintOnHololens {

	//#################### CONSTRUCTORS ####################

	LabelManager::LabelManager(size_t maxLabelCount) : m_maxLabelCount(std::min<size_t>(maxLabelCount, colours.size())) {}

	//#################### PUBLIC MEMBER FUNCTIONS ####################

	bool LabelManager::add_label(std::string& name) {
		// Make sure that a label with the specified name does not already exist.
		if (has_label(name)) return false;

		// Make sure that we're not trying to exceed the maximum number of labels.
		if (get_label_count() == get_max_label_count()) return false;

		// Add the new label.
		SpaintTriangleMesh::Label label = static_cast<SpaintTriangleMesh::Label>(m_labelAllocator.allocate());
		m_labelProperties.insert(std::make_pair(label, std::make_pair(name, colours[label])));
		m_labelsByName.insert(std::make_pair(name, label));

		return true;
	}

	SpaintTriangleMesh::Label LabelManager::get_label(const std::string& name) const {
		return MapUtil::lookup(m_labelsByName, name);
	}

	Vector3u LabelManager::get_label_colour(SpaintTriangleMesh::Label label) const {
		return MapUtil::lookup(m_labelProperties, label).second;
	}

	const std::vector<Vector3u>& LabelManager::get_label_colours() const {
		return colours;
	}

	size_t LabelManager::get_label_count() const {
		return m_labelAllocator.used_count();
	}

	std::string LabelManager::get_label_name(SpaintTriangleMesh::Label label) const {
		return MapUtil::lookup(m_labelProperties, label).first;
	}

	size_t LabelManager::get_max_label_count() const {
		return m_maxLabelCount;
	}

	SpaintTriangleMesh::Label LabelManager::get_next_label(SpaintTriangleMesh::Label label) const {
		const std::set<int>& used = m_labelAllocator.used();
		std::set<int>::const_iterator it = used.upper_bound(static_cast<int>(label));
		return it != used.end() ? static_cast<SpaintTriangleMesh::Label>(*it) : label;
	}

	SpaintTriangleMesh::Label LabelManager::get_previous_label(SpaintTriangleMesh::Label label) const {
		const std::set<int>& used = m_labelAllocator.used();
		std::set<int>::const_iterator it = used.find(static_cast<int>(label));
		return it != used.begin() ? static_cast<SpaintTriangleMesh::Label>(*--it) : label;
	}

	bool LabelManager::has_label(SpaintTriangleMesh::Label label) const {
		return m_labelProperties.find(label) != m_labelProperties.end();
	}

	bool LabelManager::has_label(const std::string& name) const {
		return m_labelsByName.find(name) != m_labelsByName.end();
	}

}
