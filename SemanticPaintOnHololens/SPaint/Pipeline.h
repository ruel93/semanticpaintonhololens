#pragma once

#include <rafl/core/RandomForest.h>
#include "Spaint\SpaintTriangleMesh.h"
#include "Content\SurfaceMesh.h"
#include "SPaint\MemoryBlock.h"
#include "rafl/base/descriptor.h"
#include "Interactor.h"
#include "LabelManager.h"
#include "LabelPropagator.h"
#include "SpaintDecisionFunctionGenerator.h"

#define THREAD_TIME

namespace SemanticPaintOnHololens {
	typedef std::vector<SpaintTriangleMesh> Triangles;
	class Pipeline {

	private:
		typedef std::shared_ptr<rafl::RandomForest<SpaintTriangleMesh::Label> > RandomForest_Ptr;

	public:
		enum Mode {
			/** In feature inspection mode, the user can move the mouse around and visualise the features at particular points in the scene. */
			MODE_FEATURE_INSPECTION,

			/** In normal mode, the user can reconstruct and manually label the scene. */
			MODE_NORMAL,

			/** In prediction mode, the random forest is used to predict labels for previously-unseen voxels. */
			MODE_PREDICTION,

			/** In propagation mode, labels supplied by the user are propagated across surfaces in the scene. */
			MODE_PROPAGATION,

			/** In smoothing mode, voxel labels are filled in based on the labels of neighbouring voxels. */
			MODE_SMOOTHING,

			/** In train-and-predict mode, we alternate training and prediction to achieve a pleasing interactive effect. */
			MODE_TRAIN_AND_PREDICT,

			/** In training mode, a random forest is trained using voxels sampled from the current raycast. */
			MODE_TRAINING,
		};

		//#################### PRIVATE VARIABLES ####################
	private:
		void PrintWstringToDebugConsole(std::wstring string) {
			OutputDebugStringW(string.c_str());
		}
		/** The label propagator. */
		SemanticPaintOnHololens::LabelPropagator_CPtr m_labelPropagator;

		/** The maximum number of triangles for which to predict labels each frame. */
		static const size_t m_maxPredictionTriangleCount = 100;

		/** The maximum number of triangles per label from which to train each frame. */
		size_t m_maxTrainingTrianglesPerLabel = 30;

		/** The mode in which the pipeline is currently running. */
		Mode m_mode;

		/** The random forest. */
		RandomForest_Ptr m_forest;

		/** The interactor that is used to interact with the Triangles objects. */
		Interactor_Ptr m_interactor;

		/* The Label Manager*/
		LabelManager_Ptr m_labelManager;

		/** Whether or not reconstruction has started yet (the tracking can only be run once it has). */
		bool																								m_reconstructionStarted;

		/** Settings for Random Forest. */
		std::map<std::string, std::string>																	m_raflSettings;

		//############################## MUTEXES #############################

		/*lock for the forest*/
		std::mutex																							m_forestLock;

		/*lock for the training MBs*/
		std::mutex																							m_trainingLock;

		/*lock for the prediction MBs*/
		std::mutex																							m_predictionLock;


		//Needed for performance timing
		std::vector<size_t>	m_samplingTimer;
		std::vector<size_t>	m_trainingTimer;
		std::vector<size_t>	m_predictionTimer;
		std::vector<size_t>	m_propagationTimer;
		float m_averageSamplingTime = 0.f, m_averageTrainingTime = 0.f, m_averagePredictionTime = 0.f, m_averagePropagationTime = 0.f;
		size_t m_minSamplingTime = 9999999, m_minTrainingTime = 999999, m_minPredictionTime = 999999, m_minPropagationTime = 999999;
		size_t m_maxSamplingTime = 0, m_maxTrainingTime = 0, m_maxPredictionTime = 0, m_maxPropagationTime = 0;

		std::mutex			m_timerMutex;

	public:
		//############################ CONSTRUCTORS #########################
		Pipeline::Pipeline();
		//#################### PUBLIC MEMBER FUNCTIONS ####################
		Mode get_mode() const;
		void reset_forest();
		void set_mode(Mode mode);
		void run_mode_specific_section(SemanticPaintOnHololensParser::SurfaceMesh &meshes);
		const Interactor_Ptr& get_interactor();
		const LabelManager_Ptr& getLabelManager();
		LabelManager_CPtr getLabelManager() const;

		struct timings {
			float average;
			size_t minValue;
			size_t maxValue;
		};

		struct allTimings {
			timings prediction;
			timings propagation;
			timings training;
			timings sampling;
		};

		allTimings getTimings();

		//#################### PRIVATE MEMBER FUNCTIONS ####################
	private:
		void initialise();
		void run_feature_inspection_section(SemanticPaintOnHololensParser::SurfaceMesh &meshes);
		bool run_prediction_section(SemanticPaintOnHololensParser::SurfaceMesh &meshes);
		bool run_propagation_section(SemanticPaintOnHololensParser::SurfaceMesh &meshes);
		void run_smoothing_section(SemanticPaintOnHololensParser::SurfaceMesh &meshes);
		void run_training_section(SemanticPaintOnHololensParser::SurfaceMesh &meshes);
	};
}