#include "pch.h"

#include "Pipeline.h"
#include "Content/RealtimeSurfaceMeshParser.h"
#include <vector>
#include <random>

#include "rafl/base/descriptor.h"
#include "rafl/core/RandomForest.h"
#include "SPaint/ForestUtil.h"
#include "SPaint/MemoryBlock.h"
#include "MemoryBlockFactory.h"
#include "LabelManager.h"

using namespace Windows::Perception::Spatial::Surfaces;
using namespace SemanticPaintOnHololensParser;
using namespace SemanticPaintOnHololens;
using namespace rafl;


Pipeline::Pipeline() {
	initialise();
}

Pipeline::Mode Pipeline::get_mode() const {
	return m_mode;
}

void Pipeline::reset_forest() {
	const size_t treeCount = 5;
	DecisionTree<SpaintTriangleMesh::Label>::Settings dtSettings(m_raflSettings);
	m_forest.reset(new RandomForest<SpaintTriangleMesh::Label>(treeCount, dtSettings));
}

SemanticPaintOnHololens::Pipeline::allTimings SemanticPaintOnHololens::Pipeline::getTimings()
{
	std::unique_lock <std::mutex> l(m_timerMutex);
	//Compute average values
	m_averagePredictionTime = 0.f;
	for (size_t i = 0; i < m_predictionTimer.size(); ++i)
		m_averagePredictionTime += m_predictionTimer[i];
	m_averagePredictionTime /= m_predictionTimer.size();

	m_averageSamplingTime = 0.f;
	for (size_t i = 0; i < m_samplingTimer.size(); ++i)
		m_averageSamplingTime += m_samplingTimer[i];
	m_averageSamplingTime /= m_samplingTimer.size();

	m_averagePropagationTime = 0.f;
	for (size_t i = 0; i < m_propagationTimer.size(); ++i)
		m_averagePropagationTime += m_propagationTimer[i];
	m_averagePropagationTime /= m_propagationTimer.size();

	m_averageTrainingTime = 0.f;
	for (size_t i = 0; i < m_trainingTimer.size(); ++i)
		m_averageTrainingTime += m_trainingTimer[i];
	m_averageTrainingTime /= m_trainingTimer.size();

	allTimings T;

	//Fill values
	T.prediction.maxValue = m_maxPredictionTime;
	T.training.maxValue = m_maxTrainingTime;
	T.propagation.maxValue = m_maxPropagationTime;

	T.prediction.minValue = m_minPredictionTime;
	T.training.minValue = m_minTrainingTime;
	T.propagation.minValue = m_minPropagationTime;

	T.prediction.average = m_averagePredictionTime;
	T.training.average = m_averageTrainingTime;
	T.propagation.average = m_averagePropagationTime;


	T.sampling.minValue = m_minSamplingTime;
	T.sampling.average = m_averageSamplingTime;
	T.sampling.maxValue = m_maxSamplingTime;


	return T;
}

void Pipeline::initialise() {
	const size_t maxLabelCountTemp = 20;
	// Set up the interactor & the LabelManager.
	m_interactor.reset(new Interactor());
	m_labelManager.reset(new LabelManager(maxLabelCountTemp));
	const size_t maxLabelCount = m_labelManager->get_max_label_count();
	std::string first_label = "background";
	m_labelManager->add_label(first_label);

	// Register the relevant decision function generators with the factory.
	DecisionFunctionGeneratorFactory<SpaintTriangleMesh::Label>::instance().register_maker(
		SpaintDecisionFunctionGenerator::get_static_type(),
		&SpaintDecisionFunctionGenerator::maker
	);

	// Set up the random forest.
	m_raflSettings.emplace("candidateCount", "128");
	m_raflSettings.emplace("decisionFunctionGeneratorParams", "12");
	m_raflSettings.emplace("decisionFunctionGeneratorType", "Spaint");
	m_raflSettings.emplace("gainThreshold", "0.0");
	m_raflSettings.emplace("maxClassSize", "10000");
	m_raflSettings.emplace("maxTreeHeight", "20");
	m_raflSettings.emplace("randomSeed", "1234");
	m_raflSettings.emplace("seenExamplesThreshold", "512");
	m_raflSettings.emplace("splittabilityThreshold", "0.5");
	m_raflSettings.emplace("usePMFReweighting", "1");

	reset_forest();
	m_forest->set_entropyValues(maxLabelCountTemp);

	m_mode = MODE_NORMAL;

	constexpr float minCosAngleBeweenNormals = 0.5f;
	constexpr float maxSqaredDistance = 0.1f; // Max 30 cm from User Painted Triangle
	constexpr UINT maxIterationCount = 100; // Max number of recursive calls
	m_labelPropagator.reset(new LabelPropagator(minCosAngleBeweenNormals, maxSqaredDistance, maxIterationCount)); //the limi
}

void Pipeline::set_mode(Mode mode) {
	m_mode = mode;
}

const Interactor_Ptr& Pipeline::get_interactor() {
	return m_interactor;
}

const LabelManager_Ptr & SemanticPaintOnHololens::Pipeline::getLabelManager()
{
	return m_labelManager;
}

LabelManager_CPtr SemanticPaintOnHololens::Pipeline::getLabelManager() const
{
	return m_labelManager;
}

void Pipeline::run_mode_specific_section(SurfaceMesh &mesh) {
	std::lock_guard<std::mutex>(mesh.get_mutex_triangles());
	switch (m_mode) {
	case MODE_FEATURE_INSPECTION:
		run_feature_inspection_section(mesh); //TODO
		mesh.UpdateTriangleColors(m_labelManager);
		break;
	case MODE_PREDICTION:
		if (run_prediction_section(mesh)) {
			mesh.UpdateTriangleColors(m_labelManager);
		}
		break;
	case MODE_PROPAGATION:
		if (run_propagation_section(mesh)) {
			mesh.UpdateTriangleColors(m_labelManager);
		}
		break;
	case MODE_SMOOTHING:
		run_smoothing_section(mesh);
		mesh.UpdateTriangleColors(m_labelManager);
		break;
	case MODE_TRAIN_AND_PREDICT:
	{
		static bool trainThisFrame = false;
		trainThisFrame = !trainThisFrame; //why?

		if (trainThisFrame) run_training_section(mesh);
		else run_prediction_section(mesh);

		break;
	}
	case MODE_TRAINING:
		run_training_section(mesh);
		break;
	default:
		break;
	}
}


void Pipeline::run_feature_inspection_section(SurfaceMesh &mesh) {
	return;
}

bool Pipeline::run_prediction_section(SurfaceMesh &mesh) {
	if (!m_forest->is_valid()) return false;

	auto& triangles = mesh.getTriangleVector();
	auto& normals = mesh.getNormals();
	auto& positions = mesh.getPositions();

	std::vector<size_t> idxs;

	//Setup and start sampling timer
	{
#ifdef THREAD_TIME
		auto t1 = std::chrono::high_resolution_clock::now();
#endif

		//Sample Triangles
		std::vector<bool> used(triangles.size(), false);
		static std::default_random_engine engine;
		std::uniform_int_distribution<> dist(0, triangles.size() - 1);
		while (idxs.size() < m_maxPredictionTriangleCount && std::find(used.begin(), used.end(), false) != used.end()) {
			size_t idx = dist(engine);
			if (used[idx])
				continue;
			if (triangles[idx].packedLabel.group == SpaintTriangleMesh::LG_FOREST || triangles[idx].packedLabel.label == static_cast<SpaintTriangleMesh::Label> (0))
				idxs.push_back(idx);
			used[idx] = true;
		}
#ifdef THREAD_TIME
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
		if (duration.count() > 0) {
			std::unique_lock <std::mutex> l(m_timerMutex);
			m_samplingTimer.push_back(static_cast <size_t>(duration.count()));
			m_maxSamplingTime = max(static_cast <size_t>(duration.count()), m_maxSamplingTime);
			m_minSamplingTime = min(static_cast <size_t>(duration.count()), m_minSamplingTime);
		}
#endif
	}

	{
		//Timer for prediction
#ifdef THREAD_TIME
		auto t1 = std::chrono::high_resolution_clock::now();
#endif
			std::vector<rafl::Descriptor_CPtr> descriptors(idxs.size());
		std::array<float, 3 * 3 + 3> featuresForDescriptor;
		// Calculate feature descriptors for the sampled triangles
		for (size_t i = 0; i < idxs.size(); ++i) {
			for (uint8 j = 0; j < 3; j++) {
				for (uint8 k = 0; k < 3; k++)
					featuresForDescriptor[j * 3 + k] = normals[triangles[idxs[i]].indexes[j]][k];
			}
			for (uint8 j = 0; j < 3; j++)
				featuresForDescriptor[9 + j] = positions[triangles[idxs[i]].indexes[j]][1];//Y component keeps height;
			descriptors[i].reset(new rafl::Descriptor(featuresForDescriptor.begin(), featuresForDescriptor.begin() + 3 * 3 + 2));
		}

		//std::vector<Descriptor_CPtr> descriptors = ForestUtil::make_descriptors(*m_predictionFeaturesMB, triangles_number, 3 * 3 + 3); //3 normals per point + height per point

		std::vector<SpaintTriangleMesh::PackedLabel> labels(idxs.size());
		std::vector<SpaintTriangleMesh::PackedLabel> finalLabels;
		std::vector<size_t> finalIdxs;

		for (size_t i = 0; i < idxs.size(); ++i) {
			labels[i] = SpaintTriangleMesh::PackedLabel(m_forest->predict(descriptors[i]), SpaintTriangleMesh::LG_FOREST);
			if (labels[i].label != static_cast<SpaintTriangleMesh::Label> (0)) {
				finalLabels.push_back(labels[i]);
				finalIdxs.push_back(idxs[i]);
			}
		}

		m_interactor->mark_triangles(finalIdxs, finalLabels, mesh);

#ifndef NO_ENTROPY
		if (mesh.hasNeighbours()) {
			for (size_t i = 0; i < finalIdxs.size(); ++i) {
				if (triangles[finalIdxs[i]].packedLabel.label != 0)
					m_labelPropagator->propagate_forest_label(triangles[idxs[i]], mesh, 0, triangles[idxs[i]].center);
			}
		}
#endif
#ifdef THREAD_TIME
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
		if (duration.count() > 0) {
			std::unique_lock <std::mutex> l(m_timerMutex);
			m_predictionTimer.push_back(static_cast <size_t>(duration.count()));
			m_maxPredictionTime = max(static_cast <size_t>(duration.count()), m_maxPredictionTime);
			m_minPredictionTime = min(static_cast <size_t>(duration.count()), m_minPredictionTime);
		}
#endif
	}
	return true;
}

bool Pipeline::run_propagation_section(SurfaceMesh &mesh) {
	if (!mesh.hasNeighbours())
		return false;
	bool flag = false;
	auto& triangles = mesh.getTriangleVector();
#ifdef THREAD_TIME
	auto t1 = std::chrono::high_resolution_clock::now();
#endif
	for (size_t i = 0; i < triangles.size(); ++i) {
		if (triangles[i].packedLabel.group == SpaintTriangleMesh::LG_USER && triangles[i].packedLabel.label != 0)
			flag = flag || m_labelPropagator->propagate_label(triangles[i], mesh, 0, triangles[i].center, triangles[i].normal);
	}
#ifdef THREAD_TIME
	auto t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
	if (duration.count() > 0) {
		std::unique_lock <std::mutex> l(m_timerMutex);
		m_propagationTimer.push_back(static_cast <size_t>(duration.count()));
		m_maxPropagationTime = max(static_cast <size_t>(duration.count()), m_maxPropagationTime);
		m_minPropagationTime = min(static_cast <size_t>(duration.count()), m_minPropagationTime);
	}
#endif
	return flag;
}

void Pipeline::run_smoothing_section(SurfaceMesh &mesh) {}

void Pipeline::run_training_section(SurfaceMesh &mesh) {
	const size_t maxLabelCount = m_labelManager->get_max_label_count();
	size_t labelCount = 0;
	std::vector<bool> labelMask(maxLabelCount, false);

	//vector to store the mask.
	for (size_t i = 1; i < maxLabelCount; ++i) {//first label is background, cannot be used for training
		if (m_labelManager->has_label(static_cast<SpaintTriangleMesh::Label>(i))) {
			labelMask[i] = true;
			++labelCount;
		}
	}

	auto& triangles = mesh.getTriangleVector();
	auto& normals = mesh.getNormals();
	auto& positions = mesh.getPositions();

	std::vector<uint16> idxs;

	//Sample Triangles
	const size_t triangles_number = labelCount * m_maxTrainingTrianglesPerLabel;
	std::vector <unsigned int> trainingTrianglesPerLabel(maxLabelCount, 0);



	//First get a vector storing only indexes of "trainable" triangles
	{
#ifdef THREAD_TIME
		auto t1 = std::chrono::high_resolution_clock::now();
#endif

		std::vector<uint16> trainableIndexes;
		trainableIndexes.reserve(triangles_number);
		for (uint16 i = 0; i < triangles.size(); ++i) {
			unsigned int label_int = static_cast<unsigned int> (triangles[i].packedLabel.label);
			if (labelMask[label_int] == true && triangles[i].packedLabel.group != SpaintTriangleMesh::LG_FOREST) {
				trainingTrianglesPerLabel[label_int] = trainingTrianglesPerLabel[label_int] + 1;
				trainableIndexes.push_back(i);
			}
		}
		//trainableIndexes.shrink_to_fit();
		//assert(triangles.size() >= trainableIndexes.size());
		//check if m_maxTrainingTrianglesPerLabel has been violated
		bool violated = false;
		for (size_t i = 0; i < maxLabelCount; ++i)
			violated = violated || (trainingTrianglesPerLabel[i] > m_maxTrainingTrianglesPerLabel);//will be set to true if at least one label constraint is violated

		if (trainableIndexes.size() <= triangles_number && !violated) {
			//No sampling needed
			idxs.resize(trainableIndexes.size());
			for (size_t i = 0; i < trainableIndexes.size(); ++i)
				idxs[i] = trainableIndexes[i];
		}
		else {
			//Here we need to sample
			std::vector<bool> used(trainableIndexes.size(), false);
			std::fill(trainingTrianglesPerLabel.begin(), trainingTrianglesPerLabel.end(), 0);//reset trainingtrianglesPerLabel
			static std::default_random_engine engine;
			std::uniform_int_distribution<> dist(0, trainableIndexes.size() - 1);
			while (idxs.size() < triangles_number && std::find(used.begin(), used.end(), false) != used.end()) {
				int idx = dist(engine);
				if (used[idx])
					continue;
				unsigned int label_int = static_cast<unsigned int> (triangles[trainableIndexes[idx]].packedLabel.label);
				if (trainingTrianglesPerLabel[label_int] < m_maxTrainingTrianglesPerLabel) {
					idxs.push_back(trainableIndexes[idx]);
					trainingTrianglesPerLabel[label_int] = trainingTrianglesPerLabel[label_int] + 1;
				}
				used[idx] = true;
			}
		}
#ifdef THREAD_TIME
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
		if (duration.count() > 0) {
			std::unique_lock <std::mutex> l(m_timerMutex);
			m_samplingTimer.push_back(static_cast <size_t>(duration.count()));
			m_maxSamplingTime = max(static_cast <size_t>(duration.count()), m_maxSamplingTime);
			m_minSamplingTime = min(static_cast <size_t>(duration.count()), m_minSamplingTime);
		}
#endif
	}
	/*
	std::vector<bool> used(triangles.size(), false);
	static std::default_random_engine engine;
	std::uniform_int_distribution<> dist(0, triangles.size() - 1);
	while (idxs.size() < triangles_number && std::find(used.begin(), used.end(), false) != used.end()) {
		int idx = dist(engine);
		if (used[idx])
			continue;
		unsigned int label_int = static_cast<unsigned int> (triangles[idx].packedLabel.label);
		if (labelMask[label_int] == true && triangles[idx].packedLabel.group != SpaintTriangleMesh::LG_FOREST) {
			if (trainingTrianglesPerLabel[label_int] < m_maxTrainingTrianglesPerLabel) {
				idxs.push_back((uint16)idx);
				trainingTrianglesPerLabel[label_int] = trainingTrianglesPerLabel[label_int] + 1;
			}
		}
		used[idx] = true;
	}*/

	if (idxs.size() == 0)
		return;


#ifdef _DEBUG
	// Output the numbers of voxels sampled for each label (for debugging purposes).
	std::wstring output = L"";
	for (size_t i = 0; i < maxLabelCount; ++i) {
		output = output + std::to_wstring(trainingTrianglesPerLabel[i]) + L" ";
	}
	output = output + L"\n";
	PrintWstringToDebugConsole(output);
#endif



	typedef std::shared_ptr<const Example<SpaintTriangleMesh::Label> > Example_CPtr;
#ifdef THREAD_TIME
	auto t1 = std::chrono::high_resolution_clock::now();
#endif
	std::vector<Example_CPtr> examples(idxs.size());
	std::array<float, 3 * 3 + 3> FeaturesForTriangle;
	for (size_t i = 0; i < idxs.size(); ++i) {
		for (uint8 j = 0; j < 3; j++) {
			for (uint8 k = 0; k < 3; k++)
				FeaturesForTriangle[j * 3 + k] = normals[triangles[idxs[i]].indexes[j]][k];
		}
		for (uint8 j = 0; j < 3; j++)
			FeaturesForTriangle[9 + j] = positions[triangles[idxs[i]].indexes[j]][1];//Y component keeps height;
		rafl::Descriptor_Ptr descriptor(new rafl::Descriptor(FeaturesForTriangle.begin(), FeaturesForTriangle.begin() + 3 * 3 + 2));
		examples[i] = Example_CPtr(new rafl::Example<SpaintTriangleMesh::Label>(descriptor, triangles[idxs[i]].packedLabel.label));
	}

	// Train the forest.
	const size_t splitBudget = 20;//The maximum number of nodes per tree that may be split in this training step.
	std::unique_lock<std::mutex> forestLock(m_forestLock);
	m_forest->add_examples(examples);
	m_forest->train(splitBudget);
#ifdef THREAD_TIME
	auto t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
	if (duration.count() > 0) {
		std::lock_guard <std::mutex> l(m_timerMutex);
		m_trainingTimer.push_back(static_cast <size_t>(duration.count()));
		m_maxTrainingTime = max(static_cast <size_t>(duration.count()), m_maxTrainingTime);
		m_minTrainingTime = min(static_cast <size_t>(duration.count()), m_minTrainingTime);
	}
#endif
}
