/**
 * spaint: ForestUtil.h
 * Copyright (c) Torr Vision Group, University of Oxford, 2015. All rights reserved.
 */

#ifndef H_SPAINT_FORESTUTIL
#define H_SPAINT_FORESTUTIL

#include "SPaint/MemoryBlock.h"

#include "rafl/examples/Example.h"

namespace SemanticPaintOnHololens {

/**
 * \brief This struct provides utility functions that can make feature descriptors and examples for use with rafl random forests.
 */
struct ForestUtil
{
  //#################### PUBLIC STATIC MEMBER FUNCTIONS ####################

  /**
   * \brief Makes rafl feature descriptors from feature descriptors that are stored implicitly and contiguously in an InfiniTAM memory block.
   *
   * \param featuresMB      The InfiniTAM memory block containing the feature descriptors.
   * \param descriptorCount The number of feature descriptors that are stored in the memory block.
   * \param featureCount    The number of features in a feature descriptor.
   * \return                The rafl feature descriptors.
   */
  static std::vector<rafl::Descriptor_CPtr> make_descriptors(const ORUtils::MemoryBlock<float>& featuresMB, size_t descriptorCount, size_t featureCount);

  /**
   * \brief Makes rafl examples from feature descriptors that are stored implicitly and contiguously in an InfiniTAM memory block.
   *
   * The memory block contains feature descriptors that are grouped by the label that should be assigned to them. In particular,
   * the block is divided into equally-sized segments, each of which contains maxDescriptorsPerLabel feature descriptors. Within
   * segment i, the first descriptorCounts[i] (<= maxDescriptorsPerLabel) feature descriptors are valid and can be used to make
   * examples. Each feature descriptor in segment i is assigned label i when making examples.
   *
   * \param featuresMB              The InfiniTAM memory block containing the feature descriptors.
   * \param descriptorCounts	    A vector containing the number of triangles sampledin per valid label.
   * \param exampleCount			The number of the sampled triangles (sum of descriptorCounts)
   * \param featureCount            The number of features in a feature descriptor.
   * \param maxDescriptorsPerLabel  The number of descriptors that could potentially be stored in a label segment. AKA the maxTrainingTrianglesPerLabel
   * \param labelCount              The number of labels for which the memory block contains descriptors.
   * \return                        The rafl examples.
   */
  template <typename Label>
  static std::vector<std::shared_ptr<const rafl::Example<Label> > > make_examples(const ORUtils::MemoryBlock<float>& featuresMB,
                                                                                    std::vector<unsigned int> descriptorCounts, 
																					size_t exampleCount,
                                                                                    size_t featureCount, size_t maxDescriptorsPerLabel, size_t labelCount)
  {
    typedef std::shared_ptr<const rafl::Example<Label> > Example_CPtr;

    // Make the examples.
    featuresMB.UpdateHostFromDevice();
    const float *features = featuresMB.GetData(MEMORYDEVICE_CPU);
    std::vector<Example_CPtr> examples(exampleCount);
    size_t exampleIndex = 0;
    for(Label label = 0; label < static_cast<Label>(labelCount); ++label)
    {
      for(size_t i = 0; i < descriptorCounts[label]; ++i)
      {
        // Copy the features for the example into a descriptor.
        const float *featuresForExample = features + (label * maxDescriptorsPerLabel + i) * featureCount;
        rafl::Descriptor_Ptr descriptor(new rafl::Descriptor(featuresForExample, featuresForExample + featureCount));

        // Make the example and add it.
        examples[exampleIndex++] = Example_CPtr(new rafl::Example<Label>(descriptor, label));
      }
    }

    return examples;
  }
};

}

#endif
