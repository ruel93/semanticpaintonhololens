#pragma once
#include "DirectXPackedVector.h"

using namespace Windows::Foundation::Numerics;
using namespace DirectX::PackedVector;

//using uchar = unsigned char;
//#define EMULATOR

namespace SemanticPaintOnHololens {
	struct SpaintTriangleMesh {

		typedef unsigned char uchar;
		typedef uchar Label;

		bool hasUpdatedColorInformation = true;
		bool hasUpdatedLabel = false;

		enum LabelGroup {
			/** Labels in the "forest" group are those that have been predicted by the random forest. They are assumed to be unreliable, and ignored for training purposes. */
			LG_FOREST,

			/** Labels in the "propagated" group are those that have been propagated from user labels. They are assumed to be reliable enough for training, but can be overwritten. */
			LG_PROPAGATED,

			/** Labels in the "user" group are those that have been supplied directly by the user. They are assumed to be correct enough to use for training. */
			LG_USER
		};

		struct PackedLabel {
			//~~~~~~~~~~~~~~~~~~~~ PUBLIC VARIABLES ~~~~~~~~~~~~~~~~~~~~

			/** The group of the label (e.g. "user", "forest", etc.). */
			uchar group : 2;

			/** The label itself. */
			uchar label : 6;

			//~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORS ~~~~~~~~~~~~~~~~~~~~
			PackedLabel() : group(LG_USER), label(0) {}

			PackedLabel(Label label_, LabelGroup group_) : group(static_cast<uchar>(group_)), label(label_) {}

			//~~~~~~~~~~~~~~~~~~~~ PUBLIC OPERATORS ~~~~~~~~~~~~~~~~~~~~

			bool operator==(const PackedLabel& rhs) const {
				return group == rhs.group && label == rhs.label;
			}
		};

		//#################### PUBLIC VARIABLES ####################


		/** Semantic label. */
		PackedLabel packedLabel;

		XMUBYTEN4 clr; //to store color, not used right now

		std::array<uint16, 3> indexes;

		std::array<std::size_t, 3> neighbours;

		float3 normal;

		float3 center;

		float radius;

		unsigned short neighbors_count = 0;

		//#################### CONSTRUCTORS ####################
		SpaintTriangleMesh(std::array<uint16, 3> _indexes) : indexes(_indexes) {
			packedLabel = PackedLabel(0, LG_USER);
#ifdef EMULATOR
			clr.x = (uint8)100;
			clr.y = (uint8)100;
			clr.z = (uint8)100;
			clr.w = (uint8)128;
#else
			clr.x = (uint8)0;
			clr.y = (uint8)0;
			clr.z = (uint8)0;
			clr.w = (uint8)255;
#endif


		}

		//SpaintTriangleMesh(SpaintTriangleMesh& other) = delete;
		//SpaintTriangleMesh& SpaintTriangleMesh::operator=(SpaintTriangleMesh& other) = delete;

	};
}