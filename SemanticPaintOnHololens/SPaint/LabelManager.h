/**
 * spaint: LabelManager.cpp
 * Copyright (c) Torr Vision Group, University of Oxford, 2015. All rights reserved.
 */

#pragma once
#include "pch.h"

#include <map>
#include <string>
#include <vector>

#include <tvgutil/IDAllocator.h>

#include "SPaint/SpaintTriangleMesh.h"


namespace SemanticPaintOnHololens {

	/**
	 * \brief An instance of this class can be used to manage the labels that are used for labelling a scene.
	 */
	class LabelManager {
		typedef std::array<unsigned char, 3> Vector3u;
		//#################### PRIVATE VARIABLES ####################
	private:
		/** The ID allocator used to allocate labels. */
		tvgutil::IDAllocator m_labelAllocator;

		/** A map from label names to labels. */
		std::map<std::string, SpaintTriangleMesh::Label> m_labelsByName;

		/** A map from labels to their properties (names and colours). */
		std::map<SpaintTriangleMesh::Label, std::pair<std::string, Vector3u> > m_labelProperties;

		/** The maximum number of labels that the manager is allowed to allocate. */
		size_t m_maxLabelCount;

		//#################### CONSTRUCTORS ####################
	public:
		/**
		 * \brief Constructs a label manager.
		 *
		 * Note: The maximum number of labels that can be specified is limited by the number of available colours (currently 20).
		 *
		 * \param maxLabelCount The maximum number of labels that the manager is allowed to allocate.
		 */
		explicit LabelManager(size_t maxLabelCount);

		//#################### PUBLIC MEMBER FUNCTIONS ####################
	public:
		/**
		 * \brief Attempts to add a label with the specified name.
		 *
		 * A label will only be added if a label with the specified name does not
		 * already exist and we have not yet reached the maximum label count.
		 *
		 * \param name  The name of the label we want to add.
		 * \return      true, if we successfully added the label, or false otherwise.
		 */
		bool add_label(std::string& name);

		/**
		 * \brief Gets the label with the specified name.
		 *
		 * \param name                The name of the label we want to get.
		 * \return                    The label with the specified name.
		 * \throws std::runtime_error If the manager does not contain a label with the specified name.
		 */
		SpaintTriangleMesh::Label get_label(const std::string& name) const;

		/**
		 * \brief Gets the colour of the specified label.
		 *
		 * \param label               The label whose colour we want to get.
		 * \return                    The colour of the specified label.
		 * \throws std::runtime_error If the manager does not contain the specified label.
		 */
		Vector3u get_label_colour(SpaintTriangleMesh::Label label) const;

		/**
		 * \brief Gets all of the available label colours.
		 *
		 * \return  The available label colours.
		 */
		const std::vector<Vector3u>& get_label_colours() const;

		/**
		 * \brief Gets the number of labels that are currently allocated.
		 *
		 * \return  The number of labels that are currently allocated.
		 */
		size_t get_label_count() const;

		/**
		 * \brief Gets the name of the specified label.
		 *
		 * \param label               The label whose name we want to get.
		 * \return                    The name of the specified label.
		 * \throws std::runtime_error If the manager does not contain the specified label.
		 */
		std::string get_label_name(SpaintTriangleMesh::Label label) const;

		/**
		 * \brief Gets the maximum number of labels that the manager is allowed to allocate.
		 *
		 * \return  The maximum number of labels that the manager is allowed to allocate.
		 */
		size_t get_max_label_count() const;

		/**
		 * \brief Gets the label directly succeeding the specified label in the label order (if any).
		 *
		 * \param label The label whose successor we want to get.
		 * \return      The label directly succeeding the specified label in the label order (if any), or the specified label if it's the last label.
		 */
		SpaintTriangleMesh::Label get_next_label(SpaintTriangleMesh::Label label) const;

		/**
		 * \brief Gets the label directly preceding the specified label in the label order (if any).
		 *
		 * \param label The label whose predecessor we want to get.
		 * \return      The label directly preceding the specified label in the label order (if any), or the specified label if it's the first label.
		 */
		SpaintTriangleMesh::Label get_previous_label(SpaintTriangleMesh::Label label) const;

		/**
		 * \brief Gets whether or not the manager contains the specified label.
		 *
		 * \param name  The label we want to check.
		 * \return      true, if the manager contains the specified label, or false otherwise.
		 */
		bool has_label(SpaintTriangleMesh::Label label) const;

		/**
		 * \brief Gets whether or not the manager contains a label with the specified name.
		 *
		 * \param name  The name of the label we want to check.
		 * \return      true, if the manager contains a label with the specified name, or false otherwise.
		 */
		bool has_label(const std::string& name) const;
	};

	//#################### TYPEDEFS ####################

	typedef std::shared_ptr<LabelManager> LabelManager_Ptr;
	typedef std::shared_ptr <const LabelManager> LabelManager_CPtr;

}
