#include <pch.h>
#include <random>
#include "LabelPropagator.h"
#include "TriangleMarker.h"
#include "Content/SurfaceMesh.h"

using namespace SemanticPaintOnHololens;
using namespace Windows::Foundation::Numerics;

//#################### CONSTRUCTORS ####################
LabelPropagator::LabelPropagator(float minCosAngleBetweenNormals, float maxSquaredDistanceFromUserTriangle, size_t maxIterations)
	: m_minCosAngleBetweenNormals(minCosAngleBetweenNormals), m_maxSquaredDistanceFromUserTriangle(maxSquaredDistanceFromUserTriangle), m_maxIterations(maxIterations) {}

bool SemanticPaintOnHololens::LabelPropagator::propagate_label(SpaintTriangleMesh &currentTriangle, SemanticPaintOnHololensParser::SurfaceMesh &surface, size_t currentIteration, float3 originalCenter, float3 originalNormal) const
{
	if (currentIteration > m_maxIterations)
		return false;
	bool flag = false;
	++currentIteration;
	SpaintTriangleMesh::Label label = currentTriangle.packedLabel.label;
	auto& normals = surface.getNormals();
	auto& positions = surface.getPositions();
	auto& triangles = surface.getTriangleVector();

#ifdef HASH_MAP

#else
	for (unsigned short i = 0; i < currentTriangle.neighbors_count; ++i) {
		auto nextTriangle = &triangles[currentTriangle.neighbours[i]];
		if (nextTriangle->packedLabel.label == label)//skip if the label is the same
			continue;

		float squareDistanceFromOriginalCenter = distance_squared(originalCenter, nextTriangle->center);
		if (squareDistanceFromOriginalCenter <= m_maxSquaredDistanceFromUserTriangle)
			continue;

		float cosAngleBetweenNormals = dot(currentTriangle.normal, nextTriangle->normal);
		if(cosAngleBetweenNormals >= m_minCosAngleBetweenNormals) {
			//Mark triangle
			std::lock_guard<std::mutex>(surface.get_mutex_triangles());
			mark_triangle(SpaintTriangleMesh::PackedLabel(label, SpaintTriangleMesh::LG_PROPAGATED), *nextTriangle);
			if (nextTriangle->packedLabel.label == label) {
				//If the label has been propagated, call recursively and set flag to true;
				propagate_label(*nextTriangle, surface, currentIteration, originalCenter, originalNormal);
				flag = true;
			}
		}
}
#endif
	return flag;

	

}

void SemanticPaintOnHololens::LabelPropagator::propagate_forest_label(SpaintTriangleMesh &currentTriangle, SemanticPaintOnHololensParser::SurfaceMesh &surface, size_t currentIteration, float3 originalCenter) const {
	if (currentIteration > m_maxIterations)
		return;
	++currentIteration;
	SpaintTriangleMesh::Label label = currentTriangle.packedLabel.label;
	auto& normals = surface.getNormals();
	auto& positions = surface.getPositions();
	auto& triangles = surface.getTriangleVector();

#ifdef HASH_MAP

#else
	for (unsigned short i = 0; i < currentTriangle.neighbors_count; ++i) {
		auto nextTriangle = &triangles[currentTriangle.neighbours[i]];
		if (nextTriangle->packedLabel.label != static_cast<SpaintTriangleMesh::Label>(0))//skip if the label is the same or has been propagated from a user one
			continue;

		float cosAngleBetweenNormals = dot(currentTriangle.normal, nextTriangle->normal);
		float squareDistanceFromOriginalCenter = distance_squared(originalCenter, nextTriangle->center);
		if (squareDistanceFromOriginalCenter <= m_maxSquaredDistanceFromUserTriangle, cosAngleBetweenNormals >= m_minCosAngleBetweenNormals) {
			//Mark triangle
			std::lock_guard<std::mutex>(surface.get_mutex_triangles());
			mark_triangle(SpaintTriangleMesh::PackedLabel(label, SpaintTriangleMesh::LG_FOREST), *nextTriangle);
			if (nextTriangle->packedLabel.label == label) { //this is false if nextTriangle has been labeled by the user.
				//If the label has been propagated, call recursively and set flag to true;
				//propagate_forest_label(*nextTriangle, surface, currentIteration, originalCenter);
			}
		}
	}
#endif
}




