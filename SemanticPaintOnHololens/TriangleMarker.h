#pragma once
#include "SPaint\SpaintTriangleMesh.h"
#include "TriangleMarker_Settings.h"
#include "SPaint\MemoryBlock.h"
#include "Content\SurfaceMesh.h"

namespace SemanticPaintOnHololens {

	/**
	* \brief An instance of a class deriving from this one can be used to mark a set of voxels with a semantic label.
	*/
	class TriangleMarker {
		//#################### DESTRUCTOR ####################
	public:
		/**
		* \brief Destroys the voxel marker.
		*/
		~TriangleMarker() {}

		//#################### PUBLIC MEMBER FUNCTIONS ####################
	public:
		/**
		* \brief Clears the labels of some or all of the voxels in an array, depending on the settings specified.
		*
		* \param voxels      The array of voxels.
		* \param voxelCount  The number of voxels in the array.
		* \param settings    The settings to use for the label-clearing operation.
		*/
		void clear_labels(SpaintTriangleMesh *triangles, int triangleCount, ClearingSettings settings) const;

		/**
		* \brief Marks a set of voxels in the scene with the specified semantic label.
		*
		* \param TriangleIndexes	A vecotr containing the indexes of the vector.
		* \param label             The semantic label with which to mark the triangles.
		* \param mode              The marking mode.
		*/
		void mark_triangles(std::vector<size_t> triangleIndexes, SpaintTriangleMesh::PackedLabel label,
			SemanticPaintOnHololensParser::SurfaceMesh& mesh,
			MarkingMode mode = NORMAL_MARKING) const;

		/**
		* \brief Marks a set of voxels in the scene with the specified semantic labels.
		*
		* \param TriangleIndexes	A vecotr containing the indexes of the vector.
		* \param voxelLabelsMB     A memory block containing the semantic labels with which to mark the triangles (one per triangle).
		* \param mode              The marking mode.
		*/
		void mark_triangles(std::vector<size_t>& triangleIndexes, std::vector<SpaintTriangleMesh::PackedLabel>& triangleLabels,
			SemanticPaintOnHololensParser::SurfaceMesh& mesh,
			MarkingMode mode = NORMAL_MARKING) const;

	};
		/**
		* \brief Decides whether or not it is possible to overwrite the old semantic label for a triangle with a new one.
		*
		* This decision is based on both the old and new labels.
		*
		* \param oldLabel  The old semantic label for the triangle.
		* \param newLabel  The new semantic label for the triangle.
		* \return          true, if it is possible to overwrite the existing label with the new one, or false otherwise.
		*/
		inline bool can_overwrite_label(SpaintTriangleMesh::PackedLabel oldLabel, SpaintTriangleMesh::PackedLabel newLabel) {
			// The new label can overwrite the old label iff one of the following is true:
			return
				// (a) The old label is the original (background) label.
				(oldLabel.label == 0 && oldLabel.group == SpaintTriangleMesh::LG_USER) ||

				// (b) The old and new labels are both non-user labels.
				(oldLabel.group != SpaintTriangleMesh::LG_USER && newLabel.group != SpaintTriangleMesh::LG_USER) ||

				// (c) The new label was supplied by the user.
				newLabel.group == SpaintTriangleMesh::LG_USER;
		}

		/**
		* \brief Clears the label of the specified voxel as necessary depending on the settings specified.
		*
		* \param voxel     The voxel whose label may be cleared.
		* \param settings  The settings to use for the label-clearing operation.
		*/
		inline void clear_label(SpaintTriangleMesh& triangle, ClearingSettings settings) {
			SpaintTriangleMesh::PackedLabel& packedLabel = triangle.packedLabel;

			bool shouldClear = false;
			switch (settings.mode) {
			case CLEAR_EQ_LABEL:            shouldClear = packedLabel.label == settings.label; break;
			case CLEAR_EQ_LABEL_NEQ_GROUP:  shouldClear = packedLabel.label == settings.label && packedLabel.group != settings.group; break;
			case CLEAR_NEQ_GROUP:           shouldClear = packedLabel.group != settings.group; break;
			default:                        shouldClear = true; break;
			}

			if (shouldClear) packedLabel = SpaintTriangleMesh::PackedLabel();
		}

		/**
		* \brief Marks a voxel in the scene with a semantic label.
		*
		* \param loc         The index of the triangle.
		* \param label       The semantic label with which to mark the triangle.
		* \param oldLabel    An optional location into which to store the old semantic label of the triangle.
		* \param mode        The marking mode.
		*/
		inline void mark_triangle(SpaintTriangleMesh::PackedLabel label, SpaintTriangleMesh& triangle, MarkingMode mode = NORMAL_MARKING) {
			SpaintTriangleMesh::PackedLabel oldLabelLocal = triangle.packedLabel;
			if (mode == FORCED_MARKING || can_overwrite_label(oldLabelLocal, label)) {
				triangle.packedLabel = label;
				triangle.hasUpdatedLabel = true;
			}
		}



}
