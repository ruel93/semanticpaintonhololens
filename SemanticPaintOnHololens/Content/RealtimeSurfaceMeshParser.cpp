//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#include "pch.h"
#include "Common\DirectXHelper.h"
#include "RealtimeSurfaceMeshParser.h"
#include <omp.h>
#include "Spaint\pipeline.h"

//#define MODE_TIME
using namespace SemanticPaintOnHololensParser;
using namespace SemanticPaintOnHololens;

using namespace Concurrency;
using namespace DX;
using namespace Windows::Foundation::Collections;
using namespace Windows::Foundation::Numerics;
using namespace Windows::Perception::Spatial;
using namespace Windows::Perception::Spatial::Surfaces;
using namespace Windows::UI::Input::Spatial;

using namespace Platform;

RealtimeSurfaceMeshParser::RealtimeSurfaceMeshParser(const std::shared_ptr<DX::DeviceResources>& deviceResources, std::shared_ptr<Pipeline> pipeline, SpatialCoordinateSystem^ staticCoordinateSystem) :
	m_deviceResources(deviceResources), m_pipeline(pipeline), m_staticCoordinateSystem(staticCoordinateSystem) {
	m_meshCollection.clear();
	CreateDeviceDependentResources();

	// Mutex to avoid multiple thread writing on the same window
	m_output_lockPTR.reset(&m_output_lock);

	//just create an empty task so that m_pipelineTask is not empty
	m_pipelineTask = concurrency::create_task([this]() {return; });
}

void RealtimeSurfaceMeshParser::setLabelOnTriangle(const SpaintTriangleMesh::PackedLabel packedLabel) {
	if (m_insightReady) {
		auto& surface = m_meshCollection.at(m_inSightTriangle.first);
		auto triangle = m_inSightTriangle.second;
		surface.setLabelOnTriangle(packedLabel, triangle, m_pipeline->getLabelManager());
	}
}

void RealtimeSurfaceMeshParser::search_insightTriangle(SpatialPointerPose^ pointerPose) {
	if (pointerPose == nullptr)
		return;
	const float3 headPosition = pointerPose->Head->Position;
	const float3 headDirection = pointerPose->Head->ForwardDirection;
	std::lock_guard<std::mutex> guard(m_meshCollectionLock);
	std::map<Platform::Guid, observedTriangle> candidates;
	for (auto iter = m_meshCollection.begin(); iter != m_meshCollection.end(); ) {
		auto& pair = *iter;
		auto& surfaceMesh = pair.second;
		auto guid = pair.first;
		if (!surfaceMesh.hasTriangles() || !surfaceMesh.GetIsActive()) {
			++iter;
			continue;
		}
		candidates.emplace(guid, surfaceMesh.get_inSightTriangle(headPosition, headDirection));
		/*
		auto SpatialSurface = surfaceMesh.get_SpatialSurfaceMesh();
		if (SpatialSurface != nullptr) {
			IBox<SpatialBoundingOrientedBox>^ box = SpatialSurface->SurfaceInfo->TryGetBounds(m_staticCoordinateSystem);
			float3 boxCentre = box->Value.Center;
			float distance = length(cross(boxCentre - headPosition, headDirection));
			//Check if the sight intercept the box of the surface
			if (distance < box->Value.Extents.x || distance < box->Value.Extents.y || distance < box->Value.Extents.z) {
				candidates.emplace(guid, surfaceMesh.get_inSightTriangle(headPosition, headDirection));
			}
		*/
		++iter;
	}
	float dist = 9999.f;
	auto idx = candidates.begin();
	m_insightReady = false;
	for (auto iter = candidates.begin(); iter != candidates.end(); ) {
		auto& pair = *iter;
		auto& candidate = pair.second;
		if (candidate.dist < dist && candidate.found) {
			dist = candidate.dist;
			idx = iter;
			m_insightReady = true;
			break;
		}
		++iter;
	}
	if(m_insightReady)
		m_inSightTriangle = *idx;
}

// Called once per frame, maintains and updates the mesh collection.
void RealtimeSurfaceMeshParser::Update(
	DX::StepTimer const& timer,
	SpatialCoordinateSystem^ coordinateSystem
) {
	std::lock_guard<std::mutex> guard(m_meshCollectionLock);

	const float timeElapsed = static_cast<float>(timer.GetTotalSeconds());

	// Update meshes as needed, based on the current coordinate system.
	// Also remove meshes that are inactive for too long.
	for (auto iter = m_meshCollection.begin(); iter != m_meshCollection.end(); ) {
		auto& pair = *iter;
		auto& surfaceMesh = pair.second;

		//surfaceMesh.set_output_lock(m_output_lockPTR);

		// Update the surface mesh.
		surfaceMesh.UpdateTransform(
			m_deviceResources->GetD3DDevice(),
			m_deviceResources->GetD3DDeviceContext(),
			timer,
			coordinateSystem
		);

		// Check to see if the mesh has expired.
		float lastActiveTime = surfaceMesh.GetLastActiveTime();
		float inactiveDuration = timeElapsed - lastActiveTime;
		if (inactiveDuration > c_maxInactiveMeshTime) {
			// Surface mesh is expired.
			m_meshCollection.erase(iter++);
		} else {
			++iter;
		}
	};
	//Only run the pipeline if the previous iteration has finished running
	if (m_pipelineTask.is_done()) {
		m_pipelineTask = concurrency::create_task([this]() {
#ifdef MODE_TIME
			auto t1 = std::chrono::high_resolution_clock::now();
#endif
			std::vector<concurrency::task<void>> tasks;
			for (auto iter = m_meshCollection.begin(); iter != m_meshCollection.end(); ) {
				auto& pair = *iter;
				auto& surfaceMesh = pair.second;
				if (!surfaceMesh.hasTriangles()) continue;
				tasks.push_back(concurrency::create_task([&]() {
					m_pipeline->run_mode_specific_section(surfaceMesh);
				}));
				++iter;
			}
			for (size_t i = 0; i < tasks.size(); ++i) {
				tasks[i].wait();// wait for all tasks on pipeline to be finished before starting the next one
			}
#ifdef MODE_TIME
			auto t2 = std::chrono::high_resolution_clock::now();
			auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
			if (duration.count() > 0) {
				auto output = std::to_wstring(duration.count());
				OutputDebugStringW(output.c_str());
				OutputDebugStringW(L", ");
			}
#endif
			tasks.clear();
		});
	}


}



void RealtimeSurfaceMeshParser::AddSurface(Guid id, SpatialSurfaceInfo^ newSurface) {
	AddOrUpdateSurfaceAsync(id, newSurface);
}

void RealtimeSurfaceMeshParser::UpdateSurface(Guid id, SpatialSurfaceInfo^ newSurface) {
	AddOrUpdateSurfaceAsync(id, newSurface);
	//OutputDebugStringW(L"\n-----------------------\nSURFACE HAS BEEN UPDATED!!!!!! ALL TRIANGLES OF THIS SURFACE WILL BE DESTROYED\n-----------------------\n");
}

Concurrency::task<void> RealtimeSurfaceMeshParser::AddOrUpdateSurfaceAsync(Guid id, SpatialSurfaceInfo^ newSurface) {
	auto options = ref new SpatialSurfaceMeshOptions();
	options->IncludeVertexNormals = true;

	// The level of detail setting is used to limit mesh complexity, by limiting the number
	// of triangles per cubic meter.
	auto createMeshTask = create_task(newSurface->TryComputeLatestMeshAsync(m_maxTrianglesPerCubicMeter, options));
	auto processMeshTask = createMeshTask.then([this, id](SpatialSurfaceMesh^ mesh) {
		if (mesh != nullptr) {
			std::lock_guard<std::mutex> guard(m_meshCollectionLock);

			auto& surfaceMesh = m_meshCollection[id];
			surfaceMesh.UpdateSurface(mesh, m_staticCoordinateSystem);
			surfaceMesh.SetIsActive(true);
		}
	}, task_continuation_context::use_current());

	return processMeshTask;
}

void RealtimeSurfaceMeshParser::RemoveSurface(Guid id) {
	std::lock_guard<std::mutex> guard(m_meshCollectionLock);
	m_meshCollection.erase(id);
}

void RealtimeSurfaceMeshParser::ClearSurfaces() {
	std::lock_guard<std::mutex> guard(m_meshCollectionLock);
	m_meshCollection.clear();
}

void RealtimeSurfaceMeshParser::HideInactiveMeshes(IMapView<Guid, SpatialSurfaceInfo^>^ const& surfaceCollection) {
	std::lock_guard<std::mutex> guard(m_meshCollectionLock);

	// Hide surfaces that aren't actively listed in the surface collection.
	for (auto& pair : m_meshCollection) {
		const auto& id = pair.first;
		auto& surfaceMesh = pair.second;

		surfaceMesh.SetIsActive(surfaceCollection->HasKey(id) ? true : false);
	};
}

// Renders one frame using the vertex, geometry, and pixel shaders.
void RealtimeSurfaceMeshParser::Render(bool isStereo, bool useWireframe) {
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete) {
		return;
	}

	auto context = m_deviceResources->GetD3DDeviceContext();

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetInputLayout(m_inputLayout.Get());

	// Attach our vertex shader.
	context->VSSetShader(
		m_vertexShader.Get(),
		nullptr,
		0
	);

	// The constant buffer is per-mesh, and will be set for each one individually.

	if (!m_usingVprtShaders) {
		// Attach the passthrough geometry shader.
		context->GSSetShader(
			m_geometryShader.Get(),
			nullptr,
			0
		);
	}

	if (useWireframe) {
		// Use a wireframe rasterizer state.
		m_deviceResources->GetD3DDeviceContext()->RSSetState(m_wireframeRasterizerState.Get());

		// Attach a pixel shader to render a solid color wireframe.
		context->PSSetShader(
			m_colorPixelShader.Get(),
			nullptr,
			0
		);
	} else {
		// Use the default rasterizer state.
		m_deviceResources->GetD3DDeviceContext()->RSSetState(m_defaultRasterizerState.Get());

		// Attach a pixel shader that can do lighting.
		context->PSSetShader(
			m_lightingPixelShader.Get(),
			nullptr,
			0
		);
	}

	{
		std::lock_guard<std::mutex> guard(m_meshCollectionLock);

		// Draw the meshes.
		auto device = m_deviceResources->GetD3DDevice();
		if (m_insightReady) {
			auto& insightID = m_inSightTriangle.first;
			for (auto& pair : m_meshCollection) {
				auto& id = pair.first;
				auto& surfaceMesh = pair.second;
				if (id == insightID) {
					surfaceMesh.Draw(device, context, m_usingVprtShaders, isStereo, m_inSightTriangle.second);
				} else {
					surfaceMesh.Draw(device, context, m_usingVprtShaders, isStereo);
				}
			}
		} else {
			for (auto& pair : m_meshCollection) {
				auto& id = pair.first;
				auto& surfaceMesh = pair.second;
				surfaceMesh.Draw(device, context, m_usingVprtShaders, isStereo);
			}
		}
	}
}

void RealtimeSurfaceMeshParser::CreateDeviceDependentResources() {
	m_usingVprtShaders = m_deviceResources->GetDeviceSupportsVprt();

	// On devices that do support the D3D11_FEATURE_D3D11_OPTIONS3::
	// VPAndRTArrayIndexFromAnyShaderFeedingRasterizer optional feature
	// we can avoid using a pass-through geometry shader to set the render
	// target array index, thus avoiding any overhead that would be
	// incurred by setting the geometry shader stage.
	std::wstring vertexShaderFileName = m_usingVprtShaders ? L"ms-appx:///SurfaceVprtVertexShader.cso" : L"ms-appx:///SurfaceVertexShader.cso";
	//m_usingVprtShaders is false on hololens emulator


	// Load shaders asynchronously.
	task<std::vector<byte>> loadVSTask = DX::ReadDataAsync(vertexShaderFileName);
	task<std::vector<byte>> loadLightingPSTask = DX::ReadDataAsync(L"ms-appx:///SimpleLightingPixelShader.cso");
	task<std::vector<byte>> loadWireframePSTask = DX::ReadDataAsync(L"ms-appx:///SolidColorPixelShader.cso");

	task<std::vector<byte>> loadGSTask;
	if (!m_usingVprtShaders) {
		// Load the pass-through geometry shader.
		loadGSTask = DX::ReadDataAsync(L"ms-appx:///SurfaceGeometryShader.cso");
	}

	// After the vertex shader file is loaded, create the shader and input layout.
	auto createVSTask = loadVSTask.then([this](const std::vector<byte>& fileData) {
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateVertexShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&m_vertexShader
			)
		);

		static const D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R16G16B16A16_SNORM, 0,	D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL",   0, DXGI_FORMAT_R8G8B8A8_SNORM,     1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR",    0, DXGI_FORMAT_R8G8B8A8_UNORM,	 2, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateInputLayout(
				vertexDesc,
				ARRAYSIZE(vertexDesc),
				&fileData[0],
				fileData.size(),
				&m_inputLayout
			)
		);
	});

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createLightingPSTask = loadLightingPSTask.then([this](const std::vector<byte>& fileData) {
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&m_lightingPixelShader
			)
		);
	});

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createWireframePSTask = loadWireframePSTask.then([this](const std::vector<byte>& fileData) {
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&m_colorPixelShader
			)
		);
	});

	task<void> createGSTask;
	if (!m_usingVprtShaders) {
		// After the pass-through geometry shader file is loaded, create the shader.
		createGSTask = loadGSTask.then([this](const std::vector<byte>& fileData) {
			DX::ThrowIfFailed(
				m_deviceResources->GetD3DDevice()->CreateGeometryShader(
					&fileData[0],
					fileData.size(),
					nullptr,
					&m_geometryShader
				)
			);
		});
	}

	// Once all shaders are loaded, create the mesh.
	task<void> shaderTaskGroup = m_usingVprtShaders ?
		(createLightingPSTask && createWireframePSTask && createVSTask) :
		(createLightingPSTask && createWireframePSTask && createVSTask && createGSTask);

	// Once the cube is loaded, the object is ready to be rendered.
	auto finishLoadingTask = shaderTaskGroup.then([this]() {

		// Recreate device-based surface mesh resources.
		std::lock_guard<std::mutex> guard(m_meshCollectionLock);
		for (auto& iter : m_meshCollection) {
			iter.second.ReleaseDeviceDependentResources();
			iter.second.CreateDeviceDependentResources(m_deviceResources->GetD3DDevice());
		}

		// Create a default rasterizer state descriptor.
		D3D11_RASTERIZER_DESC rasterizerDesc = CD3D11_RASTERIZER_DESC(D3D11_DEFAULT);

		// Create the default rasterizer state.
		m_deviceResources->GetD3DDevice()->CreateRasterizerState(&rasterizerDesc, m_defaultRasterizerState.GetAddressOf());

		// Change settings for wireframe rasterization.
		rasterizerDesc.AntialiasedLineEnable = true;
		rasterizerDesc.CullMode = D3D11_CULL_NONE;
		rasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;

		// Create a wireframe rasterizer state.
		m_deviceResources->GetD3DDevice()->CreateRasterizerState(&rasterizerDesc, m_wireframeRasterizerState.GetAddressOf());

		m_loadingComplete = true;
	});
}

void RealtimeSurfaceMeshParser::ReleaseDeviceDependentResources() {
	m_loadingComplete = false;

	m_vertexShader.Reset();
	m_inputLayout.Reset();
	m_geometryShader.Reset();
	m_lightingPixelShader.Reset();
	m_colorPixelShader.Reset();

	m_defaultRasterizerState.Reset();
	m_wireframeRasterizerState.Reset();

	std::lock_guard<std::mutex> guard(m_meshCollectionLock);
	for (auto& iter : m_meshCollection) {
		iter.second.ReleaseDeviceDependentResources();
	}
}

bool RealtimeSurfaceMeshParser::HasSurface(Platform::Guid id) {
	std::lock_guard<std::mutex> guard(m_meshCollectionLock);
	return m_meshCollection.find(id) != m_meshCollection.end();
}

Windows::Foundation::DateTime RealtimeSurfaceMeshParser::GetLastUpdateTime(Platform::Guid id) {
	std::lock_guard<std::mutex> guard(m_meshCollectionLock);
	auto& meshIter = m_meshCollection.find(id);
	if (meshIter != m_meshCollection.end()) {
		auto const& mesh = meshIter->second;
		return mesh.GetLastUpdateTime();
	} else {
		static const Windows::Foundation::DateTime zero;
		return zero;
	}
}
