//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#include "pch.h"

#include <ppltasks.h>
#include <numeric>

#include "Common\DirectXHelper.h"
#include "Common\StepTimer.h"
#include "GetDataFromIBuffer.h"
#include "SurfaceMesh.h"
#include "SPaint\SpaintTriangleMesh.h"
#include "TriangleMarker.h"
#include "DirectXPackedVector.h"


#ifdef _DEBUG
#include <cassert>
#endif // DEBUG


using namespace SemanticPaintOnHololensParser;
using namespace DirectX;
using namespace DirectX::PackedVector;
using namespace Windows::Perception::Spatial;
using namespace Windows::Perception::Spatial::Surfaces;
using namespace Windows::Foundation::Numerics;
using namespace Windows::Storage::Streams;
using namespace Platform;
using namespace SemanticPaintOnHololens;


SurfaceMesh::SurfaceMesh() {
	std::lock_guard<std::mutex> lock(m_meshResourcesMutex);
	ReleaseDeviceDependentResources();
	m_lastUpdateTime.UniversalTime = 0;
	m_triangles.reserve(5000);
	m_triangleCreationTask = concurrency::create_task([this]() {return; });
}

SurfaceMesh::~SurfaceMesh() {
	std::lock_guard<std::mutex> lock(m_meshResourcesMutex);

	ReleaseDeviceDependentResources();
}

void SurfaceMesh::UpdateSurface(
	SpatialSurfaceMesh^ surfaceMesh, SpatialCoordinateSystem^ staticCoordinateSystem) {
	m_surfaceMesh = surfaceMesh;
	m_staticCoordinateSystem = staticCoordinateSystem;
	m_updateNeeded = true;
	m_trianglesCreated = false;
	m_updatedColorsReady = false;
}

void SurfaceMesh::UpdateDeviceBasedResources(
	ID3D11Device* device) {
	std::lock_guard<std::mutex> lock(m_meshResourcesMutex);

	ReleaseDeviceDependentResources();
	CreateDeviceDependentResources(device);
}

observedTriangle SurfaceMesh::get_inSightTriangle(const float3 headPosition, const float3 headDirection) {
	observedTriangle triangle;
	triangle.found = false;
	float previousDist = 9999.f;
	for (size_t i = 0; i < m_triangles.size(); ++i) {
		if (dot(m_triangles[i].normal, headDirection) <= -0.001f) continue;
		float dist = dot(-m_triangles[i].normal, headPosition - m_triangles[i].center) / dot(m_triangles[i].normal, headDirection);
		if (dist <= 0.f && dist > previousDist)
			continue;
		float3 intersectionPoint = headPosition + dist * headDirection;
		if (isnan(intersectionPoint.x)) continue;
		if (distance(m_triangles[i].center, intersectionPoint) > m_triangles[i].radius) continue;


		//calculates 3 vector for the lines
		float3 A = float3(m_globalPositions[m_triangles[i].indexes[0]][0], m_globalPositions[m_triangles[i].indexes[0]][1], m_globalPositions[m_triangles[i].indexes[0]][2]);
		float3 B = float3(m_globalPositions[m_triangles[i].indexes[1]][0], m_globalPositions[m_triangles[i].indexes[1]][1], m_globalPositions[m_triangles[i].indexes[1]][2]);
		float3 C = float3(m_globalPositions[m_triangles[i].indexes[2]][0], m_globalPositions[m_triangles[i].indexes[2]][1], m_globalPositions[m_triangles[i].indexes[2]][2]);
		float3 AB = B - A;
		float3 AC = C - A;
		float3 BC = C - B;
#define P intersectionPoint
		float3 AP = P - A;
		float3 BP = P - B;
		// (AC cross AP) dot (AP cross AB) . This is > 0 only if the point is contained between the 2 lines.
#define checksign( x, y, z ) (dot(cross(x , y), cross(y, z)))
		//if it is contained between 2 pair of line, it is inside the triangle
		if (checksign(AC, AP, AB) > 0 && checksign(BC, BP, -AB) > 0) {
			triangle.dist = dist;
			triangle.found = true;
			triangle.intersectionPoint = intersectionPoint;
			triangle.triangleIndex = i;
			previousDist = dist;
		}
	}

	return triangle; //if 0 is returned, no triangle is intersecated
}

void SurfaceMesh::setLabelOnTriangle(const SpaintTriangleMesh::PackedLabel packedLabel, const observedTriangle triangle, LabelManager_Ptr labelManager) {
	std::lock_guard<std::mutex>lock(m_trianglesMutex);
	auto actualTriangle = &m_triangles[triangle.triangleIndex];
	mark_triangle(packedLabel, *actualTriangle, FORCED_MARKING);//These labels are set by the user, so they can overwrite previously set labels

	//To reduce overhead simply update this triangle color
	auto clr = labelManager->get_label_colour(actualTriangle->packedLabel.label);
	actualTriangle->clr.x = static_cast<uint8>(clr[0]);
	actualTriangle->clr.y = static_cast<uint8>(clr[1]);
	actualTriangle->clr.z = static_cast<uint8>(clr[2]);
	actualTriangle->hasUpdatedColorInformation = true;
	actualTriangle->hasUpdatedLabel = false;

	m_colorUpdateNeeded = true;
}

void SurfaceMesh::setLabelOnTriangle(const SpaintTriangleMesh::PackedLabel packedLabel, const float3 headPosition, const float3 headDirection, const float maxDistance) {
#pragma omp parallel for
	for (size_t i = 0; i < m_triangles.size(); ++i) {
		float distance = length(cross(m_triangles[i].center - headPosition, headDirection));//distance point from vector
		if (distance < maxDistance) {
			std::lock_guard<std::mutex>lock(m_trianglesMutex);
			mark_triangle(packedLabel, m_triangles[i], FORCED_MARKING);//These labels are set by the user, so they can overwrite previously set labels
		}
	}
	//implementation based on single points
	/*
	std::vector<uint16>marked_indexes;

	for (uint16 i = 0; i < m_globalPositions.size(); ++i) {
		float3 vertex = { m_globalPositions[i][0], m_globalPositions[i][1], m_globalPositions[i][2] };
		float distance = length(cross(vertex - headPosition, headDirection));
		if (distance < maxDistance)
			marked_indexes.push_back(i);
	}

#pragma omp parallel for
	for (size_t i = 0; i < m_triangles.size(); ++i) {
		for (size_t j = 0; j < marked_indexes.size(); ++j) {
			if (m_triangles[i].indexes[0] == marked_indexes[j] || m_triangles[i].indexes[1] == marked_indexes[j] || m_triangles[i].indexes[2] == marked_indexes[j])
				mark_triangle(packedLabel, m_triangles[i]);
		}
	}
	*/
}

// Spatial Mapping surface meshes each have a transform. This transform is updated every frame.
void SurfaceMesh::UpdateTransform(
	ID3D11Device* device,
	ID3D11DeviceContext* context,
	DX::StepTimer const& timer,
	SpatialCoordinateSystem^ baseCoordinateSystem) {
	if (m_surfaceMesh == nullptr) {
		// Not yet ready.
		m_isActive = false;
	}

	if (m_updateNeeded) {
		CreateVertexResources(device);
		m_updateNeeded = false;
	}
	else {
		std::lock_guard<std::mutex> lock(m_meshResourcesMutex);

		if (m_updateReady) {
			// Surface mesh resources are created off-thread so that they don't affect rendering latency.
			// When a new update is ready, we should begin using the updated vertex position, normal, and 
			// index buffers.
			SwapVertexBuffers();
			m_updateReady = false;

		}
	}

	/*if (m_trianglesCreated) {
		if (m_colorUpdateNeeded) {
			//concurrency::create_task([this, device]() {//Update colors in an async way
			m_colorUpdateNeeded = false;
			std::lock_guard<std::mutex> lock(m_meshResourcesMutex);
			UpdateColorBuffer(device);
			//});
		}
		

	}
	else if (m_newResourcesCreated) {
		CreateTriangleResources();
	}*/
	if(m_newResourcesCreated) 
		CreateTriangleResources();
	if (m_trianglesCreated) {
		if (m_colorUpdateNeeded) {
			//concurrency::create_task([this, device]() {//Update colors in an async way
			m_colorUpdateNeeded = false;
			UpdateColorBuffer(device);
			//});
		}
	}


	if (m_updatedColorsReady) {
		std::lock_guard<std::mutex> lock(m_meshResourcesMutex);
		m_updatedColorsReady = false;
		SwapColorBuffer();
	}
	//if (!m_trianglesCreated)//Create triangles if they are not available
	//	CreateTriangleResources();

	// If the surface is active this frame, we need to update its transform.
	XMMATRIX transform;
	if (m_isActive) {
		// The transform is updated relative to a SpatialCoordinateSystem. In the SurfaceMesh class, we
		// expect to be given the same SpatialCoordinateSystem that will be used to generate view
		// matrices, because this class uses the surface mesh for rendering.
		// Other applications could potentially involve using a SpatialCoordinateSystem from a stationary
		// reference frame that is being used for physics simulation, etc.
		auto tryTransform = m_surfaceMesh->CoordinateSystem->TryGetTransformTo(baseCoordinateSystem);
		if (tryTransform != nullptr) {
			// If the transform can be acquired, this spatial mesh is valid right now and
			// we have the information we need to draw it this frame.
			transform = XMLoadFloat4x4(&tryTransform->Value);
			m_lastActiveTime = static_cast<float>(timer.GetTotalSeconds());
		}
		else {
			// If the transform is not acquired, the spatial mesh is not valid right now
			// because its location cannot be correlated to the current space.
			m_isActive = false;
		}
	}

	if (!m_isActive) {
		// If for any reason the surface mesh is not active this frame - whether because
		// it was not included in the observer's collection, or because its transform was
		// not located - we don't have the information we need to update it.
		return;
	}

	// Set up a transform from surface mesh space, to world space.
	XMMATRIX scaleTransform = XMMatrixScalingFromVector(XMLoadFloat3(&m_surfaceMesh->VertexPositionScale));
	XMStoreFloat4x4(
		&m_constantBufferData.modelToWorld,
		XMMatrixTranspose(scaleTransform * transform)
	);

	// Surface meshes come with normals, which are also transformed from surface mesh space, to world space.
	XMMATRIX normalTransform = transform;
	// Normals are not translated, so we remove the translation component here.
	normalTransform.r[3] = XMVectorSet(0.f, 0.f, 0.f, XMVectorGetW(normalTransform.r[3]));
	XMStoreFloat4x4(
		&m_constantBufferData.normalToWorld,
		XMMatrixTranspose(normalTransform)
	);

	if (!m_constantBufferCreated) {
		// If loading is not yet complete, we cannot actually update the graphics resources.
		// This return is intentionally placed after the surface mesh updates so that this
		// code may be copied and re-used for CPU-based processing of surface data.
		CreateDeviceDependentResources(device);
		return;
	}

	context->UpdateSubresource(
		m_modelTransformBuffer.Get(),
		0,
		NULL,
		&m_constantBufferData,
		0,
		0
	);
}

void SurfaceMesh::RequestColorUpdate() {
	m_colorUpdateNeeded = true;
}

void SurfaceMesh::CreateTriangleResources() {
	if (m_updateNeeded)
		return;
	XMMATRIX transform;

	auto tryTransform = m_surfaceMesh->CoordinateSystem->TryGetTransformTo(m_staticCoordinateSystem);
	if (tryTransform != nullptr) {
		// If the transform can be acquired, this spatial mesh is valid right now and
		// we have the information we need to draw it this frame.
		transform = XMLoadFloat4x4(&tryTransform->Value);
	}
	else {
		// If the transform is not acquired, the spatial mesh is not valid right now
		// because its location cannot be correlated to the current space.
		return;
	}

	// Set up a transform from surface mesh space, to world space.
	XMMATRIX scaleTransform = XMMatrixScalingFromVector(XMLoadFloat3(&m_surfaceMesh->VertexPositionScale));
	//XMMATRIX vertexTransform = XMMatrixTranspose(scaleTransform * transform);
	XMMATRIX vertexTransform = scaleTransform * transform;


	// Surface meshes come with normals, which are also transformed from surface mesh space, to world space.
	XMMATRIX normalTransform = transform;
	// Normals are not translated, so we remove the translation component here.
	normalTransform.r[3] = XMVectorSet(0.f, 0.f, 0.f, XMVectorGetW(normalTransform.r[3]));
	//normalTransform = XMMatrixTranspose(normalTransform);

	std::vector<std::array<float, 3>> globalPositions, globalNormals; //to store positions in float
	std::vector<uint16> globalIndices; //to store index

	auto pos_count = m_surfaceMesh->VertexPositions->ElementCount;
	auto nor_count = m_surfaceMesh->VertexNormals->ElementCount;
	auto ind_count = m_surfaceMesh->TriangleIndices->ElementCount;

	//uint16 *temp_index = new uint16[ind_count];
	uint16 *temp_index = (uint16*)GetDataFromIBuffer(m_indices);
	for (size_t i = 0; i < ind_count; ++i) {
		globalIndices.push_back(temp_index[i]);
	}

	//XMSHORTN4 *rawVertexData = new XMSHORTN4[pos_count];
	XMSHORTN4 *rawVertexData = (XMSHORTN4*)GetDataFromIBuffer(m_positions); //read a normilized integer from the buffer
	for (size_t i = 0; i < pos_count; ++i) {
		std::array<float, 3> positions;
		XMSHORTN4 currentPos = XMSHORTN4(rawVertexData[i]);
		XMVECTOR VertexData = XMLoadShortN4(&currentPos); //Convert into float
		VertexData.m128_f32[3] = 1.f; //The last element has to be equal to 1
		XMVECTOR ScaledVertexData = XMVector4Transform(VertexData, vertexTransform); //Apply the transformation matrix
		for (int i = 0; i < 3; ++i)
			positions[i] = ScaledVertexData.m128_f32[i];
		globalPositions.push_back(positions);
	}

	//XMBYTEN4 *rawNormalData = new XMBYTEN4[nor_count];
	XMBYTEN4 *rawNormalData = (XMBYTEN4*)GetDataFromIBuffer(m_normals); //read a normilized integer from the buffer
	for (size_t i = 0; i < nor_count; ++i) {
		std::array<float, 3> normals;
		XMBYTEN4 currentNor = XMBYTEN4(rawNormalData[i]);
		XMVECTOR NormalData = XMLoadByteN4(&currentNor); //Convert into float
		XMVECTOR ScaledNormalData = XMVector4Transform(NormalData, normalTransform); //Apply the transformation matrix
		for (int i = 0; i < 3; ++i)
			normals[i] = ScaledNormalData.m128_f32[i];
		globalNormals.push_back(normals);
	}

	if (m_updateNeeded)
		return;

	//Add Triangle in a safe way
	{
		std::lock_guard<std::mutex> lock(m_trianglesMutex);
		//First remove old version of triangles
		m_triangles.clear();
		std::array<uint16, 3> temp_indices;
		for (size_t i = 0; i < globalIndices.size(); ++i) {
			temp_indices[i % 3] = globalIndices[i];
			if (i % 3 == 2)
				m_triangles.emplace_back(move(temp_indices));
		}
		m_globalNormals = globalNormals;
		m_globalPositions = globalPositions;
	}
	setTriangle_Normal_Center();
	m_colorUpdateNeeded = true;
	m_trianglesCreated = true;
	AssignNeighbours();
	m_newResourcesCreated = false;
}

inline void SurfaceMesh::AssignNeighbours() {
#ifdef HASH_MAP

	for (size_t i = 0; i < m_triangles.size(); ++i) {
		std::pair<uint16, uint16> indexes;
		indexes.first = m_triangles[i].indexes[0];
		indexes.second = m_triangles[i].indexes[1];
		if (m_hashMap.count(indexes) == 0) {
			m_hashMap.emplace()
		}

		indexes.first = m_triangles[i].indexes[0];
		indexes.second = m_triangles[i].indexes[2];

		indexes.first = m_triangles[i].indexes[1];
		indexes.second = m_triangles[i].indexes[2];
	}
#else
	std::mutex triangleLock;

	for (size_t i = 0; i < m_triangles.size(); ++i) {

#pragma omp parallel for schedule(static) shared(triangleLock)
		for (size_t j = i + 1; j < m_triangles.size(); ++j) {
			if (m_triangles[i].neighbors_count == 3)
				break;
			if (m_triangles[j].neighbors_count == 3)
				continue;
			int count = 0;
			for (uint16 k = 0; k < 3; ++k) {
				for (uint16 w = 0; w < 3; ++w) {
					if (m_triangles[i].indexes[k] == m_triangles[j].indexes[w])
						++count;
				}
			}
			if (count == 2) {
				m_triangles[j].neighbours[m_triangles[j].neighbors_count] = i;
				m_triangles[j].neighbors_count += 1;
				std::lock_guard<std::mutex> lock(triangleLock);
				m_triangles[i].neighbours[m_triangles[i].neighbors_count] = j;
				m_triangles[i].neighbors_count += 1;
			}
		}
	}
#endif



	m_neighboursReady = true;
}

void SurfaceMesh::setTriangle_Normal_Center()
{
	std::unique_lock<std::mutex>(m_trianglesMutex);
	for (size_t i = 0; i < m_triangles.size(); ++i) {
		float3 A = float3(m_globalPositions[m_triangles[i].indexes[0]][0], m_globalPositions[m_triangles[i].indexes[0]][1], m_globalPositions[m_triangles[i].indexes[0]][2]);
		float3 B = float3(m_globalPositions[m_triangles[i].indexes[1]][0], m_globalPositions[m_triangles[i].indexes[1]][1], m_globalPositions[m_triangles[i].indexes[1]][2]);
		float3 C = float3(m_globalPositions[m_triangles[i].indexes[2]][0], m_globalPositions[m_triangles[i].indexes[2]][1], m_globalPositions[m_triangles[i].indexes[2]][2]);
		float3 AB = normalize(B - A);
		float3 AC = normalize(C - A);
		m_triangles[i].normal = cross(AB, AC);
#ifdef _DEBUG
		assert(m_triangles[i].normal.x != NAN);
		assert(m_triangles[i].normal.y != NAN);
		assert(m_triangles[i].normal.z != NAN);
#endif // _DEBUG

		m_triangles[i].center = (A + B + C) / 3;
		m_triangles[i].radius = max(distance(A, m_triangles[i].center), max(distance(B, m_triangles[i].center), distance(C, m_triangles[i].center)));
	}
}

// Does an indexed, instanced draw call after setting the IA stage to use the mesh's geometry, and
// after setting up the constant buffer for the surface mesh.
// The caller is responsible for the rest of the shader pipeline.
void SurfaceMesh::Draw(
	ID3D11Device* device,
	ID3D11DeviceContext* context,
	bool usingVprtShaders,
	bool isStereo) {
	if (!m_constantBufferCreated || !m_loadingComplete) {
		// Resources are still being initialized.
		return;
	}

	if (!m_isActive) {
		// Mesh is not active this frame, and should not be drawn.
		return;
	}

	// The vertices are provided in {vertex, normal, color} format

	UINT strides[] = { m_meshProperties.vertexStride, m_meshProperties.normalStride, 4 };
	UINT offsets[] = { 0, 0, 0 };
	ID3D11Buffer* buffers[] = { m_vertexPositions.Get(), m_vertexNormals.Get(), m_vertexColor.Get() };

	context->IASetVertexBuffers(
		0,
		ARRAYSIZE(buffers),
		buffers,
		strides,
		offsets
	);

	context->IASetIndexBuffer(
		m_triangleIndices.Get(),
		m_meshProperties.indexFormat,
		0
	);

	context->VSSetConstantBuffers(
		0,
		1,
		m_modelTransformBuffer.GetAddressOf()
	);

	if (!usingVprtShaders) {
		context->GSSetConstantBuffers(
			0,
			1,
			m_modelTransformBuffer.GetAddressOf()
		);
	}

	context->PSSetConstantBuffers(
		0,
		1,
		m_modelTransformBuffer.GetAddressOf()
	);

	context->DrawIndexedInstanced(
		m_meshProperties.indexCount,       // Index count per instance.
		isStereo ? 2 : 1,   // Instance count.
		0,                  // Start index location.
		0,                  // Base vertex location.
		0                   // Start instance location.
	);
}

void SurfaceMesh::Draw(
	ID3D11Device* device,
	ID3D11DeviceContext* context,
	bool usingVprtShaders,
	bool isStereo,
	observedTriangle triangle) {
	if (!m_constantBufferCreated || !m_loadingComplete) {
		// Resources are still being initialized.
		return;
	}

	if (!m_isActive) {
		// Mesh is not active this frame, and should not be drawn.
		return;
	}
	if (!triangle.found) {
		Draw(device, context, usingVprtShaders, isStereo);
		return;
	}
	//Generate Temporary Buffer
	Microsoft::WRL::ComPtr<ID3D11Buffer> updatedVertexColor;
	std::lock_guard<std::mutex> lock(m_meshResourcesMutex);
	std::lock_guard<std::mutex> lock2(m_trianglesMutex);
	for (uint16 j = 0; j < 3; ++j)
		m_ColorsArray.get()[m_triangles[triangle.triangleIndex].indexes[j]] = ReverseColor(m_ColorsArray.get()[m_triangles[triangle.triangleIndex].indexes[j]]);

	CD3D11_BUFFER_DESC bufferDescription(m_meshProperties.indexCount * 4, D3D11_BIND_VERTEX_BUFFER); // 16 byte per component
	D3D11_SUBRESOURCE_DATA bufferBytes = { reinterpret_cast<byte*>(m_ColorsArray.get()), 0, 0 };
	DX::ThrowIfFailed(device->CreateBuffer(&bufferDescription, &bufferBytes, updatedVertexColor.GetAddressOf()));
	// The vertices are provided in {vertex, normal, color} format

	UINT strides[] = { m_meshProperties.vertexStride, m_meshProperties.normalStride, 4 };
	UINT offsets[] = { 0, 0, 0 };
	ID3D11Buffer* buffers[] = { m_vertexPositions.Get(), m_vertexNormals.Get(), updatedVertexColor.Get() };

	context->IASetVertexBuffers(
		0,
		ARRAYSIZE(buffers),
		buffers,
		strides,
		offsets
	);

	context->IASetIndexBuffer(
		m_triangleIndices.Get(),
		m_meshProperties.indexFormat,
		0
	);

	context->VSSetConstantBuffers(
		0,
		1,
		m_modelTransformBuffer.GetAddressOf()
	);

	if (!usingVprtShaders) {
		context->GSSetConstantBuffers(
			0,
			1,
			m_modelTransformBuffer.GetAddressOf()
		);
	}

	context->PSSetConstantBuffers(
		0,
		1,
		m_modelTransformBuffer.GetAddressOf()
	);

	context->DrawIndexedInstanced(
		m_meshProperties.indexCount,       // Index count per instance.
		isStereo ? 2 : 1,   // Instance count.
		0,                  // Start index location.
		0,                  // Base vertex location.
		0                   // Start instance location.
	);
	for (uint16 j = 0; j < 3; ++j)
		m_ColorsArray.get()[m_triangles[triangle.triangleIndex].indexes[j]] = ReverseColor(m_ColorsArray.get()[m_triangles[triangle.triangleIndex].indexes[j]]);

}

XMUBYTEN4 SurfaceMesh::ReverseColor(XMUBYTEN4 originalColor) {
	XMUBYTEN4 newColor;
	newColor.x = (uint8)255 - originalColor.x;
	newColor.y = (uint8)255 - originalColor.y;
	newColor.z = (uint8)255 - originalColor.z;
	return newColor;
}

void SurfaceMesh::UpdateColorBuffer(ID3D11Device* device) {
	std::lock_guard<std::mutex> lock(m_meshResourcesMutex);
	if (device == nullptr) {
		m_colorUpdateNeeded = true;
		return;
	}
	Microsoft::WRL::ComPtr<ID3D11Buffer> updatedVertexColor;

	for (size_t i = 0; i < m_triangles.size(); ++i) {
		if (m_triangles[i].hasUpdatedColorInformation) {
			for (uint16 j = 0; j < 3; ++j)
				m_ColorsArray.get()[m_triangles[i].indexes[j]] = m_triangles[i].clr;
			m_triangles[i].hasUpdatedColorInformation = false;
		}

	}

	CD3D11_BUFFER_DESC bufferDescription(m_meshProperties.indexCount * 4, D3D11_BIND_VERTEX_BUFFER); // 16 byte per component
	D3D11_SUBRESOURCE_DATA bufferBytes = { reinterpret_cast<byte*>(m_ColorsArray.get()), 0, 0 };
	DX::ThrowIfFailed(device->CreateBuffer(&bufferDescription, &bufferBytes, updatedVertexColor.GetAddressOf()));
	m_updatedVertexColor.Swap(updatedVertexColor);
	m_updatedColorsReady = true;
}

void SurfaceMesh::UpdateTriangleColors(LabelManager_Ptr labelManager) {
	bool flagColorUpdate = false;
	for (size_t i = 0; i < m_triangles.size(); ++i) {
		if (m_triangles[i].hasUpdatedLabel) {
			auto clr = labelManager->get_label_colour(m_triangles[i].packedLabel.label);
			m_triangles[i].clr.x = static_cast<uint8>(clr[0]);
			m_triangles[i].clr.y = static_cast<uint8>(clr[1]);
			m_triangles[i].clr.z = static_cast<uint8>(clr[2]);
			flagColorUpdate = true;
			m_triangles[i].hasUpdatedColorInformation = true;
			m_triangles[i].hasUpdatedLabel = false;
		}
	}

	m_colorUpdateNeeded = flagColorUpdate;
}

void SurfaceMesh::SwapColorBuffer() {
	m_vertexColor = m_updatedVertexColor;
	m_updatedVertexColor.Reset();
}

void SurfaceMesh::CreateDirectXBuffer(
	ID3D11Device* device,
	D3D11_BIND_FLAG binding,
	IBuffer^ buffer,
	ID3D11Buffer** target) {
	CD3D11_BUFFER_DESC bufferDescription(buffer->Length, binding);
	D3D11_SUBRESOURCE_DATA bufferBytes = { GetDataFromIBuffer(buffer), 0, 0 };
	device->CreateBuffer(&bufferDescription, &bufferBytes, target);
}

void SurfaceMesh::CreateVertexResources(
	ID3D11Device* device) {
	m_trianglesCreated = false;
	m_newResourcesCreated = false;
	if (m_surfaceMesh == nullptr) {
		// Not yet ready.
		m_isActive = false;
		return;
	}

	if (m_surfaceMesh->TriangleIndices->ElementCount < 3) {
		// Not enough indices to draw a triangle.
		m_isActive = false;
		return;
	}

	// Surface mesh resources are created off-thread, so that they don't affect rendering latency.'
	auto taskOptions = Concurrency::task_options();
	auto task = concurrency::create_task([this, device]() {

		// Create new Direct3D device resources for the updated buffers. These will be set aside
		// for now, and then swapped into the active slot next time the render loop is ready to draw.

		// First, we acquire the raw data buffers.
		Windows::Storage::Streams::IBuffer^ positions = m_surfaceMesh->VertexPositions->Data;
		Windows::Storage::Streams::IBuffer^ normals = m_surfaceMesh->VertexNormals->Data;
		Windows::Storage::Streams::IBuffer^ indices = m_surfaceMesh->TriangleIndices->Data;

		//read from the buffer to create the triangles
		DataReader^ positions_reader = DataReader::FromBuffer(positions);	// DXGI_FORMAT_R16G16B16A16_SNORM (13). A four-component, 64-bit signed-normalized-integer format that supports 16 bits per channel including alpha.
		DataReader^ normals_reader = DataReader::FromBuffer(normals);		// DXGI_FORMAT_R8G8B8A8_SNORM (31). A four-component, 32-bit signed-normalized-integer format that supports 8 bits per channel including alpha.
		DataReader^ indices_reader = DataReader::FromBuffer(indices);		// DXGI_FORMAT_R16_UINT (57). A single-component, 16-bit unsigned-integer format that supports 16 bits for the red channel.

		// Then, we create Direct3D device buffers with the mesh data provided by HoloLens.
		Microsoft::WRL::ComPtr<ID3D11Buffer> updatedVertexPositions;
		Microsoft::WRL::ComPtr<ID3D11Buffer> updatedVertexNormals;
		Microsoft::WRL::ComPtr<ID3D11Buffer> updatedTriangleIndices;
		CreateDirectXBuffer(device, D3D11_BIND_VERTEX_BUFFER, positions, updatedVertexPositions.GetAddressOf());
		CreateDirectXBuffer(device, D3D11_BIND_VERTEX_BUFFER, normals, updatedVertexNormals.GetAddressOf());
		CreateDirectXBuffer(device, D3D11_BIND_INDEX_BUFFER, indices, updatedTriangleIndices.GetAddressOf());



		// Before updating the meshes, check to ensure that there wasn't a more recent update.
		{
			std::lock_guard<std::mutex> lock(m_meshResourcesMutex);

			m_ColorsArray.reset(new XMUBYTEN4[m_surfaceMesh->TriangleIndices->ElementCount]);
			//XMUBYTEN4* tempColorBuffer = new XMUBYTEN4[m_surfaceMesh->TriangleIndices->ElementCount];

			Microsoft::WRL::ComPtr<ID3D11Buffer> updatedVertexColors;

			for (size_t i = 0; i < m_surfaceMesh->TriangleIndices->ElementCount; ++i) {
				m_ColorsArray.get()[i].x = (uint8)230;
				m_ColorsArray.get()[i].y = (uint8)20;
				m_ColorsArray.get()[i].z = (uint8)20;
				m_ColorsArray.get()[i].w = (uint8)128;
			}

			CD3D11_BUFFER_DESC bufferDescription(m_surfaceMesh->TriangleIndices->ElementCount * 4, D3D11_BIND_VERTEX_BUFFER); // 4 byte per component
			D3D11_SUBRESOURCE_DATA bufferBytes = { reinterpret_cast<byte*>(m_ColorsArray.get()), 0, 0 };
			DX::ThrowIfFailed(device->CreateBuffer(&bufferDescription, &bufferBytes, updatedVertexColors.GetAddressOf()));

			auto meshUpdateTime = m_surfaceMesh->SurfaceInfo->UpdateTime;
			if (meshUpdateTime.UniversalTime > m_lastUpdateTime.UniversalTime) {

				// Prepare to swap in the new meshes.
				// Here, we use ComPtr.Swap() to avoid unnecessary overhead from ref counting.
				m_updatedVertexPositions.Swap(updatedVertexPositions);
				m_updatedVertexNormals.Swap(updatedVertexNormals);
				m_updatedTriangleIndices.Swap(updatedTriangleIndices);

				m_updatedVertexColor.Swap(updatedVertexColors);

				m_updatedPositions = positions;
				m_updatedNormals = normals;
				m_updatedIndices = indices;

				// Cache properties for the buffers we will now use.
				m_updatedMeshProperties.vertexStride = m_surfaceMesh->VertexPositions->Stride;
				m_updatedMeshProperties.normalStride = m_surfaceMesh->VertexNormals->Stride;
				m_updatedMeshProperties.indexCount = m_surfaceMesh->TriangleIndices->ElementCount;
				m_updatedMeshProperties.indexFormat = static_cast<DXGI_FORMAT>(m_surfaceMesh->TriangleIndices->Format);

				//Save it directly in XMFLOAT3
				m_updatedMeshProperties.vertexPositionScale = XMFLOAT3(m_surfaceMesh->VertexPositionScale.x, m_surfaceMesh->VertexPositionScale.y, m_surfaceMesh->VertexPositionScale.z);
				// Send a signal to the render loop indicating that new resources are available to use.
				m_updateReady = true;
				m_updatedColorsReady = true;
				m_lastUpdateTime = meshUpdateTime;
				m_loadingComplete = true;
				m_newResourcesCreated = true;
			}
		}
	});
}

void SurfaceMesh::CreateDeviceDependentResources(
	ID3D11Device* device) {
	CreateVertexResources(device);

	// Create a constant buffer to control mesh position.
	CD3D11_BUFFER_DESC constantBufferDesc(sizeof(ModelNormalConstantBuffer), D3D11_BIND_CONSTANT_BUFFER);
	DX::ThrowIfFailed(
		device->CreateBuffer(
			&constantBufferDesc,
			nullptr,
			&m_modelTransformBuffer
		)
	);

	m_constantBufferCreated = true;
}

void SurfaceMesh::ReleaseVertexResources() {
	m_vertexPositions.Reset();
	m_vertexNormals.Reset();
	m_triangleIndices.Reset();
	m_vertexColor.Reset();
}

void SurfaceMesh::SwapVertexBuffers() {
	// Swap out the previous vertex position, normal, and index buffers, and replace
	// them with up-to-date buffers.
	m_vertexPositions = m_updatedVertexPositions;
	m_vertexNormals = m_updatedVertexNormals;
	m_triangleIndices = m_updatedTriangleIndices;

	m_positions = m_updatedPositions;
	m_normals = m_updatedNormals;
	m_indices = m_updatedIndices;

	// Swap out the metadata: index count, index format, .
	m_meshProperties = m_updatedMeshProperties;

	ZeroMemory(&m_updatedMeshProperties, sizeof(SurfaceMeshProperties));
	m_updatedVertexPositions.Reset();
	m_updatedVertexNormals.Reset();
	m_updatedTriangleIndices.Reset();
}

void SurfaceMesh::ReleaseDeviceDependentResources() {
	// Clear out any pending resources.
	SwapVertexBuffers();
	SwapColorBuffer();

	// Clear out active resources.
	ReleaseVertexResources();

	m_modelTransformBuffer.Reset();

	m_constantBufferCreated = false;
	m_loadingComplete = false;
}
