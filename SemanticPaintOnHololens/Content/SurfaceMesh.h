//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#pragma once

//#define HASH_MAP

#include <vector>

#include "Common\DirectXHelper.h"
#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "ShaderStructures.h"
#include "SPaint\SpaintTriangleMesh.h"
#include "SPaint\LabelManager.h"

#ifdef HASH_MAP
#include "HashMap.h"
#endif

#include <pch.h>


namespace SemanticPaintOnHololensParser {
	struct SurfaceMeshProperties {
		unsigned int vertexStride = 0;
		unsigned int normalStride = 0;
		unsigned int indexCount = 0;
		DXGI_FORMAT  indexFormat = DXGI_FORMAT_UNKNOWN;
		DirectX::XMFLOAT3 vertexPositionScale;
	};

	struct observedTriangle {
		bool found;
		float dist;
		float3 intersectionPoint;
		size_t triangleIndex;
	};

	class SurfaceMesh final {
	public:
		SurfaceMesh();
		~SurfaceMesh();

		bool SurfaceMesh::hasNeighbours() { return m_neighboursReady; }
		bool SurfaceMesh::hasTriangles() { return m_trianglesCreated; }

		void UpdateSurface(Windows::Perception::Spatial::Surfaces::SpatialSurfaceMesh^ surface, Windows::Perception::Spatial::SpatialCoordinateSystem^ staticCoordinateSystem);
		void UpdateDeviceBasedResources(ID3D11Device* device);

		//Get the triangle that is intersected by the User sight
		observedTriangle get_inSightTriangle(const float3 headPosition, const float3 headDirection);
		void UpdateTransform(
			ID3D11Device* device,
			ID3D11DeviceContext* context,
			DX::StepTimer const& timer,
			Windows::Perception::Spatial::SpatialCoordinateSystem^ baseCoordinateSystem
		);

		void RequestColorUpdate();

		void Draw(ID3D11Device* device, ID3D11DeviceContext* context, bool usingVprtShaders, bool isStereo);

		void Draw(ID3D11Device * device, ID3D11DeviceContext * context, bool usingVprtShaders, bool isStereo, observedTriangle triangle);

		void UpdateTriangleColors(SemanticPaintOnHololens::LabelManager_Ptr labelManager);

		void CreateVertexResources(ID3D11Device* device);
		void CreateDeviceDependentResources(ID3D11Device* device);
		void ReleaseVertexResources();
		void ReleaseDeviceDependentResources();

		const bool&                             GetIsActive()       const { return m_isActive; }
		const float&                            GetLastActiveTime() const { return m_lastActiveTime; }
		const Windows::Foundation::DateTime&    GetLastUpdateTime() const { return m_lastUpdateTime; }

		void SetIsActive(const bool& isActive) { m_isActive = isActive; }

		std::vector<SemanticPaintOnHololens::SpaintTriangleMesh>& getTriangleVector() { return m_triangles; }
		std::vector<std::array<float, 3>>& getNormals() { return m_globalNormals; }
		std::vector <std::array<float, 3>>& getPositions() { return m_globalPositions; }

		std::mutex& get_mutex_triangles() { return m_trianglesMutex; }

		void setLabelOnTriangle(const SemanticPaintOnHololens::SpaintTriangleMesh::PackedLabel packedLabel, const observedTriangle triangle, SemanticPaintOnHololens::LabelManager_Ptr labelManager);
		void setLabelOnTriangle(const SemanticPaintOnHololens::SpaintTriangleMesh::PackedLabel packedLabel, const Windows::Foundation::Numerics::float3 headPosition, const Windows::Foundation::Numerics::float3 headDirection, const float maxDistance);

		Windows::Perception::Spatial::Surfaces::SpatialSurfaceMesh^ get_SpatialSurfaceMesh() { return m_surfaceMesh; }

	private:
		XMUBYTEN4 ReverseColor(XMUBYTEN4 originalColor);
		Windows::Perception::Spatial::SpatialCoordinateSystem^	m_staticCoordinateSystem;
		bool m_neighboursReady = false;
#ifdef HASH_MAP
		tools::upair_unordered_set<uint16>					m_hashMap;
#endif

		void CreateTriangleResources();
		inline void AssignNeighbours();
		void setTriangle_Normal_Center();
		void SwapVertexBuffers();
		void CreateDirectXBuffer(
			ID3D11Device* device,
			D3D11_BIND_FLAG binding,
			Windows::Storage::Streams::IBuffer^ buffer,
			ID3D11Buffer** target
		);

		//std::vector<std::array<float, 3>> m_global_positions; //to export in matlab

		Windows::Storage::Streams::IBuffer^ m_positions;
		Windows::Storage::Streams::IBuffer^ m_normals;
		Windows::Storage::Streams::IBuffer^ m_indices;
		Windows::Storage::Streams::IBuffer^ m_updatedPositions;
		Windows::Storage::Streams::IBuffer^ m_updatedNormals;
		Windows::Storage::Streams::IBuffer^ m_updatedIndices;

		Windows::Perception::Spatial::Surfaces::SpatialSurfaceMesh^ m_surfaceMesh = nullptr;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexPositions;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexNormals;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_triangleIndices;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_updatedVertexPositions;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_updatedVertexNormals;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_updatedTriangleIndices;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_modelTransformBuffer;

		std::unique_ptr<XMUBYTEN4>				m_ColorsArray;
		Microsoft::WRL::ComPtr<ID3D11Buffer>	m_vertexColor;
		Microsoft::WRL::ComPtr<ID3D11Buffer>	m_updatedVertexColor;
		bool									m_updatedColorsReady = false;
		bool									m_colorUpdateNeeded = true;

		void UpdateColorBuffer(ID3D11Device* device);
		void SwapColorBuffer();

		Windows::Foundation::DateTime m_lastUpdateTime;

		SurfaceMeshProperties m_meshProperties;
		SurfaceMeshProperties m_updatedMeshProperties;

		ModelNormalConstantBuffer m_constantBufferData;

		bool	m_constantBufferCreated = false;
		bool	m_loadingComplete = false;
		bool	m_updateNeeded = false;
		bool	m_updateReady = false;
		bool	m_isActive = false;
		bool	m_trianglesCreated = false;
		bool	m_newResourcesCreated = false;

		float	m_lastActiveTime = -1.f;
		std::vector<std::array<float, 3>>									m_globalPositions;
		std::vector<std::array<float, 3>>									m_globalNormals;

		std::mutex															m_meshResourcesMutex;
		std::mutex															m_trianglesMutex;

		std::vector<SemanticPaintOnHololens::SpaintTriangleMesh>			m_triangles;

		std::shared_ptr<std::mutex> m_output_lock;//lock for console output
		std::mutex m_positions_lock;
		Concurrency::task<void>												m_triangleCreationTask;
	public:

		void set_output_lock(std::shared_ptr<std::mutex> output_lock) { m_output_lock = output_lock; } //TO LOCK OUTPUT
	};
}
