#pragma once
#include <pch.h>
#include <unordered_set>

namespace tools {


	//Filip Srajer filip.srajer@inf.ethz.ch

	////////////////////////////////////////////////////
	///////////////   Declarations   ///////////////////
	////////////////////////////////////////////////////

	/// Used for hashing lists of more standard types.
	template <class T>
	void hash_combine(std::size_t & seed, const T & v);

	/// Hashing functor for std::pair which gives the same hash for {i,j} and {j,i}
	template <class T>
	class UnorderedPairHash {
	public:
		std::size_t operator()(const std::pair<T, T>& v) const;
	};

	/// Equal functor for std::pair which says that for {i,j} and {j,i} are equal
	template <class T>
	class UnorderedPairEqual {
	public:
		bool operator()(const std::pair<T, T>& a, const std::pair<T, T>& b) const;
	};

	template<typename T>
	using upair_unordered_set = std::unordered_set < std::pair<T, T>, UnorderedPairHash<T>, UnorderedPairEqual<T> >;

	////////////////////////////////////////////////////
	///////////////   Definitions   ////////////////////
	////////////////////////////////////////////////////


	template <class T>
	void hash_combine(std::size_t & seed, const T & v) {
		std::hash<T> hasher;
		seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	}

	template <class T>
	std::size_t UnorderedPairHash<T>::operator()(const std::pair<T, T>& v) const {
		std::size_t seed = 0;
		if (v.first < v.second)
		{
			hash_combine(seed, v.first);
			hash_combine(seed, v.second);
		}
		else
		{
			hash_combine(seed, v.second);
			hash_combine(seed, v.first);
		}
		return seed;
	}

	template <class T>
	bool UnorderedPairEqual<T>::operator()(const std::pair<T, T>& a, const std::pair<T, T>& b) const {
		return (a.first == b.first && a.second == b.second) || (a.first == b.second && a.second == b.first);
	}
}