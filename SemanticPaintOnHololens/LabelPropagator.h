#pragma once
#include "MemoryBlockFactory.h"
#include "SPaint\SpaintTriangleMesh.h"
#include "Content\SurfaceMesh.h"

namespace SemanticPaintOnHololens {
	class LabelPropagator {
		//#################### PROTECTED VARIABLES ####################
	protected:
		/** The largest angle allowed between the normals of opposite vertexes of neighbouring triangles if propagation is to occur. */
		const float m_minCosAngleBetweenNormals;

		/** The maximum squared distance allowed between the positions of opposite vertexes of neighbouring triangles if propagation is to occur. */
		const float m_maxSquaredDistanceFromUserTriangle;

		const size_t m_maxIterations;

		//#################### CONSTRUCTORS ####################
	public:
		LabelPropagator(float maxAngleBetweenNormals, float maxSquaredDistanceBetweenVoxels, size_t maxIterations);

		//#################### PUBLIC MEMBER FUNCTIONS ####################
	public:
		/**
		* \brief Propagates the specified label across the scene, stopping at position, normal or colour discontinuities.
		*
		* \param label         The label to propagate.
		* \param raycastResult The raycast result.
		* \param scene         The scene.
		*/
		bool propagate_label(SpaintTriangleMesh &triangle, SemanticPaintOnHololensParser::SurfaceMesh &surface, size_t current_iteration, float3 originalCenter, float3 originalNormal) const;
		void propagate_forest_label(SpaintTriangleMesh & currentTriangle, SemanticPaintOnHololensParser::SurfaceMesh & surface, size_t currentIteration, float3 originalCenter) const;
	};

	typedef std::shared_ptr<const LabelPropagator> LabelPropagator_CPtr;
}