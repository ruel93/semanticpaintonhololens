/**
* spaint: SpaintDecisionFunctionGenerator.cpp
* Copyright (c) Torr Vision Group, University of Oxford, 2015. All rights reserved.
*/
#include "pch.h"
#include "SpaintDecisionFunctionGenerator.h"

#include <rafl/decisionfunctions/FeatureThresholdingDecisionFunctionGenerator.h>
#include <rafl/decisionfunctions/PairwiseOpAndThresholdDecisionFunctionGenerator.h>
using namespace rafl;

namespace SemanticPaintOnHololens {

	//#################### CONSTRUCTORS ####################

	SpaintDecisionFunctionGenerator::SpaintDecisionFunctionGenerator(){
		std::pair<int, int> normalFeatureIndexRange(0, 11);
		std::pair<int, int> heightFeatureIndexRange(9, 11);

		this->add_generator(DecisionFunctionGenerator_CPtr(new FeatureThresholdingDecisionFunctionGenerator<Label>(normalFeatureIndexRange)));
		this->add_generator(DecisionFunctionGenerator_CPtr(new PairwiseOpAndThresholdDecisionFunctionGenerator<Label>(normalFeatureIndexRange)));

		this->add_generator(DecisionFunctionGenerator_CPtr(new FeatureThresholdingDecisionFunctionGenerator<Label>(heightFeatureIndexRange)));
		this->add_generator(DecisionFunctionGenerator_CPtr(new PairwiseOpAndThresholdDecisionFunctionGenerator<Label>(heightFeatureIndexRange)));
	}

	//#################### PUBLIC STATIC MEMBER FUNCTIONS ####################

	std::string SpaintDecisionFunctionGenerator::get_static_type() {
		return "Spaint";
	}

	SpaintDecisionFunctionGenerator::DecisionFunctionGenerator_Ptr SpaintDecisionFunctionGenerator::maker(const std::string& params) {
		return DecisionFunctionGenerator_Ptr(new SpaintDecisionFunctionGenerator());
	}

	//#################### PUBLIC MEMBER FUNCTIONS ####################

	std::string SpaintDecisionFunctionGenerator::get_params() const {
		return nullptr;
	}

	std::string SpaintDecisionFunctionGenerator::get_type() const {
		return get_static_type();
	}

}