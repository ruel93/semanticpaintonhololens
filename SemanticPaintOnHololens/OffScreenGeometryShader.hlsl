struct GeometryShaderInput
{
	min16float4 screenPos   : SV_POSITION;
	min16float4	color       : COLOR0;
};

struct GeometryShaderOutput
{
	min16float4 screenPos   : SV_POSITION;
	min16float4	color       : COLOR0;
};

[maxvertexcount(3)]
void main(triangle GeometryShaderInput input[3], inout TriangleStream<GeometryShaderOutput> outStream){
	GeometryShaderOutput element;
	[unroll(3)]
	for (uint i = 0; i < 3; i++)
	{
		element.screenPos = input[i].screenPos;
		element.color = input[i].color;
		outStream.Append(element);
	}
}