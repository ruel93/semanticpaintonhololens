//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#include <pch.h>

//#define BOX_2D
//#define NO_VOICE
#include "SemanticPaintOnHololensMain.h"
#include "Common\DirectXHelper.h"
#include <filesystem>

#include <windows.graphics.directx.direct3d11.interop.h>
#include <Collection.h>

#include "SPaint/Pipeline.h"

#include "Content\TextRenderer.h"
#include "Content\QuadRenderer.h"

using namespace SemanticPaintOnHololens;
using namespace SemanticPaintOnHololensParser;
using namespace HolographicTagAlong;

using namespace concurrency;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Foundation::Numerics;
using namespace Windows::Graphics::DirectX;
using namespace Windows::Graphics::Holographic;
using namespace Windows::Perception::Spatial;
using namespace Windows::Perception::Spatial::Surfaces;
using namespace Windows::UI::Input::Spatial;
using namespace Windows::Media::SpeechRecognition;
using namespace std::placeholders;

using namespace std::tr2::sys;

// Loads and initializes application assets when the application is loaded.
SemanticPaintOnHololensMain::SemanticPaintOnHololensMain(
	const std::shared_ptr<DX::DeviceResources>& deviceResources) :
	m_deviceResources(deviceResources) {
	// Register to be notified if the device is lost or recreated.
	m_deviceResources->RegisterDeviceNotify(this);
#ifndef NO_VOICE
	m_language = ref new Windows::Globalization::Language("en-US");
	CreateSpeechConstraintForLabelling();
	CreateSpeechConstraintForModeSelection();
#endif
	//Spaint pipeline
	m_pipeline.reset(new Pipeline());
}



void inline SemanticPaintOnHololensMain::CreateSpeechConstraintForModeSelection() {
	try {
		// initialize Speech Command Recognizer	
		m_ModeSpeechRecognizer = ref new SpeechRecognizer(m_language);
	}
	catch (Platform::Exception^ ex)
	{

		static const unsigned int HResultPrivacyStatementDeclined = 0x80045509;
		static const unsigned int HResultRecognizerNotFound = 0x8004503a;
		if ((unsigned int)ex->HResult == HResultRecognizerNotFound)
		{
			OutputDebugStringW(L"Speech Language pack for selected language not installed.");
		}
		else
		{
			std::wstring msg = std::wstring(begin(ex->Message), end(ex->Message));
			OutputDebugStringW(msg.c_str());
		}
	}
	//Initiliaze COnstraints
	m_speechCommandList = ref new Platform::Collections::Vector<String^>();
	m_speechCommandData.clear();
	m_speechCommandList->Append(StringReference(L"inspection"));
	m_speechCommandData.push_back(Pipeline::MODE_FEATURE_INSPECTION);
	m_speechCommandList->Append(StringReference(L"normal"));
	m_speechCommandData.push_back(Pipeline::MODE_NORMAL);
	m_speechCommandList->Append(StringReference(L"prediction"));
	m_speechCommandData.push_back(Pipeline::MODE_PREDICTION);
	m_speechCommandList->Append(StringReference(L"propagation"));
	m_speechCommandData.push_back(Pipeline::MODE_PROPAGATION);
	m_speechCommandList->Append(StringReference(L"smoothing"));
	m_speechCommandData.push_back(Pipeline::MODE_SMOOTHING);
	m_speechCommandList->Append(StringReference(L"train and predict"));
	m_speechCommandData.push_back(Pipeline::MODE_TRAIN_AND_PREDICT);
	m_speechCommandList->Append(StringReference(L"training"));
	m_speechCommandData.push_back(Pipeline::MODE_TRAINING);
	m_speechCommandList->Append(StringReference(L"scan"));
	m_speechCommandData.push_back(Pipeline::MODE_NORMAL);
	m_speechCommandList->Append(StringReference(L"get timer"));
	m_speechCommandData.push_back(Pipeline::MODE_NORMAL);

	SpeechRecognitionListConstraint^ spConstraint = ref new SpeechRecognitionListConstraint(m_speechCommandList);
	m_ModeSpeechRecognizer->Constraints->Clear();
	m_ModeSpeechRecognizer->Constraints->Append(spConstraint);

	create_task(m_ModeSpeechRecognizer->CompileConstraintsAsync()).then([this](SpeechRecognitionCompilationResult^ compilationResult) {
		if (compilationResult->Status == SpeechRecognitionResultStatus::Success) {
			m_ModeSpeechRecognizer->ContinuousRecognitionSession->StartAsync();
		}
		else {
			OutputDebugStringW(L"\nCannot start SpeechRecognizer.\n");
		}
	});

	m_ModeSpeechRecognizer->ContinuousRecognitionSession->ResultGenerated += ref new TypedEventHandler<SpeechContinuousRecognitionSession^, SpeechContinuousRecognitionResultGeneratedEventArgs^>(std::bind(&SemanticPaintOnHololensMain::OnModeSelectionResultGenerated, this, _1, _2));
}

void inline SemanticPaintOnHololensMain::CreateSpeechConstraintForLabelling() {
	try {
		// initialize Speech Command Recognizer	
		m_labelSpeechRecognizer = ref new SpeechRecognizer(m_language);
	}
	catch (Platform::Exception^ ex)
	{

		static const unsigned int HResultPrivacyStatementDeclined = 0x80045509;
		static const unsigned int HResultRecognizerNotFound = 0x8004503a;
		if ((unsigned int)ex->HResult == HResultRecognizerNotFound)
		{
			OutputDebugStringW(L"Speech Language pack for selected language not installed.");
		}
		else
		{
			std::wstring msg = std::wstring(begin(ex->Message), end(ex->Message));
			OutputDebugStringW(msg.c_str());
		}
	}
	//Initiliaze COnstraints
	m_LabelDictionaryList = ref new Platform::Collections::Vector<String^>();
	m_LabelDictionaryList->Append(StringReference(L"chair"));
	m_LabelDictionaryList->Append(StringReference(L"table"));
	m_LabelDictionaryList->Append(StringReference(L"lamp"));
	m_LabelDictionaryList->Append(StringReference(L"sofa"));
	m_LabelDictionaryList->Append(StringReference(L"bed"));
	m_LabelDictionaryList->Append(StringReference(L"armchair"));
	m_LabelDictionaryList->Append(StringReference(L"bookshelf"));
	m_LabelDictionaryList->Append(StringReference(L"clock"));
	m_LabelDictionaryList->Append(StringReference(L"desk"));
	m_LabelDictionaryList->Append(StringReference(L"wardrobe"));
	m_LabelDictionaryList->Append(StringReference(L"floor"));
	m_LabelDictionaryList->Append(StringReference(L"monitor"));
	m_LabelDictionaryList->Append(StringReference(L"wall"));

	SpeechRecognitionListConstraint^ spConstraint = ref new SpeechRecognitionListConstraint(m_LabelDictionaryList);
	m_labelSpeechRecognizer->Constraints->Clear();
	m_labelSpeechRecognizer->Constraints->Append(spConstraint);

	create_task(m_labelSpeechRecognizer->CompileConstraintsAsync()).then([this](SpeechRecognitionCompilationResult^ compilationResult) {
		if (compilationResult->Status == SpeechRecognitionResultStatus::Success) {
			OutputDebugStringW(L"\nLabel recognition ready to start.\n");
			m_labelSpeechRecognizer->ContinuousRecognitionSession->StartAsync();
		}
		else {
			OutputDebugStringW(L"\nCannot start SpeechRecognizer.\n");
		}
	});

	m_labelSpeechRecognizer->ContinuousRecognitionSession->ResultGenerated += ref new TypedEventHandler<SpeechContinuousRecognitionSession^, SpeechContinuousRecognitionResultGeneratedEventArgs^>(std::bind(&SemanticPaintOnHololensMain::OnLabelResultGenerated, this, _1, _2));
}

void inline SemanticPaintOnHololensMain::printTimingsFromPipeline(){
	auto times = m_pipeline->getTimings();
	PrintWstringToDebugConsole(L"\n------- Execution Times Per Thread --------\n");
	PrintWstringToDebugConsole(L"Prediction times.\nAverage: " + std::to_wstring(times.prediction.average) + L"\nMin: " + std::to_wstring(times.prediction.minValue) + L"\nMax: " + std::to_wstring(times.prediction.maxValue) + L"\n");
	PrintWstringToDebugConsole(L"\n--------------------\n");
	PrintWstringToDebugConsole(L"Propagation times.\nAverage: " + std::to_wstring(times.propagation.average) + L"\nMin: " + std::to_wstring(times.propagation.minValue) + L"\nMax: " + std::to_wstring(times.propagation.maxValue) + L"\n");
	PrintWstringToDebugConsole(L"\n--------------------\n");
	PrintWstringToDebugConsole(L"Training times.\nAverage: " + std::to_wstring(times.training.average) + L"\nMin: " + std::to_wstring(times.training.minValue) + L"\nMax: " + std::to_wstring(times.training.maxValue) + L"\n");
	PrintWstringToDebugConsole(L"\n--------------------\n");
	PrintWstringToDebugConsole(L"Sampling times.\nAverage: " + std::to_wstring(times.sampling.average) + L"\nMin: " + std::to_wstring(times.sampling.minValue) + L"\nMax: " + std::to_wstring(times.sampling.maxValue) + L"\n");
}

// Change the active mode in the pipeline, if we get a valid result.
void SemanticPaintOnHololensMain::OnModeSelectionResultGenerated(SpeechContinuousRecognitionSession ^sender, SpeechContinuousRecognitionResultGeneratedEventArgs ^args) {
	if (args->Result->RawConfidence > 0.3f) {
		m_lastCommand = args->Result->Text;
	}
}

void SemanticPaintOnHololensMain::OnLabelResultGenerated(SpeechContinuousRecognitionSession ^sender, SpeechContinuousRecognitionResultGeneratedEventArgs ^args) {
	if (args->Result->RawConfidence > 0.3f) {
		analyze_label_recognition(args->Result->Text);
	}
}

void SemanticPaintOnHololensMain::SetHolographicSpace(
	HolographicSpace^ holographicSpace) {
	UnregisterHolographicEventHandlers();

	m_holographicSpace = holographicSpace;

	// Initialize the input handler.
	m_spatialInputHandler = std::make_unique<SpatialInputHandler>();
	
	// For click interaction
	m_spatialInputHandler = std::make_unique<SpatialInputHandler>();

	// Use the default SpatialLocator to track the motion of the device.
	m_locator = SpatialLocator::GetDefault();

	//Set a static referenceframe
	m_staticReferenceFrame = m_locator->CreateStationaryFrameOfReferenceAtCurrentLocation();
	
	// Initialize the mesh processor
	m_meshParser = std::make_unique<RealtimeSurfaceMeshParser>(m_deviceResources, m_pipeline, m_staticReferenceFrame->CoordinateSystem);
	
	// This sample responds to changes in the positional tracking state by cancelling deactivation 
	// of positional tracking.
	m_positionalTrackingDeactivatingToken =
		m_locator->PositionalTrackingDeactivating +=
		ref new Windows::Foundation::TypedEventHandler<SpatialLocator^, SpatialLocatorPositionalTrackingDeactivatingEventArgs^>(
			std::bind(&SemanticPaintOnHololensMain::OnPositionalTrackingDeactivating, this, _1, _2)
			);


	// Respond to camera added events by creating any resources that are specific
	// to that camera, such as the back buffer render target view.
	// When we add an event handler for CameraAdded, the API layer will avoid putting
	// the new camera in new HolographicFrames until we complete the deferral we created
	// for that handler, or return from the handler without creating a deferral. This
	// allows the app to take more than one frame to finish creating resources and
	// loading assets for the new holographic camera.
	// This function should be registered before the app creates any HolographicFrames.
	m_cameraAddedToken =
		m_holographicSpace->CameraAdded +=
		ref new Windows::Foundation::TypedEventHandler<HolographicSpace^, HolographicSpaceCameraAddedEventArgs^>(
			std::bind(&SemanticPaintOnHololensMain::OnCameraAdded, this, _1, _2)
			);

	// Respond to camera removed events by releasing resources that were created for that
	// camera.
	// When the app receives a CameraRemoved event, it releases all references to the back
	// buffer right away. This includes render target views, Direct2D target bitmaps, and so on.
	// The app must also ensure that the back buffer is not attached as a render target, as
	// shown in DeviceResources::ReleaseResourcesForBackBuffer.
	m_cameraRemovedToken =
		m_holographicSpace->CameraRemoved +=
		ref new Windows::Foundation::TypedEventHandler<HolographicSpace^, HolographicSpaceCameraRemovedEventArgs^>(
			std::bind(&SemanticPaintOnHololensMain::OnCameraRemoved, this, _1, _2)
			);

	// This code sample uses a DeviceAttachedFrameOfReference to have the Spatial Mapping surface observer
	// follow along with the device's location.
	m_referenceFrame = m_locator->CreateAttachedFrameOfReferenceAtCurrentHeading();

	// Notes on spatial tracking APIs:
	// * Stationary reference frames are designed to provide a best-fit position relative to the
	//   overall space. Individual positions within that reference frame are allowed to drift slightly
	//   as the device learns more about the environment.
	// * When precise placement of individual holograms is required, a SpatialAnchor should be used to
	//   anchor the individual hologram to a position in the real world - for example, a point the user
	//   indicates to be of special interest. Anchor positions do not drift, but can be corrected; the
	//   anchor will use the corrected position starting in the next frame after the correction has
	//   occurred.


#ifdef BOX_2D
	//////////////////////////////////////// 2DBOX ///////////////////////////////////////
	m_quadRenderer = std::make_unique<QuadRenderer>(m_deviceResources);

	// Initialize the text renderer.
	constexpr unsigned int offscreenRenderTargetWidth = 2048;
	constexpr unsigned int offscreenRenderTargetHeight = 2048;
	m_textRenderer = std::make_unique<TextRenderer>(m_deviceResources, offscreenRenderTargetWidth, offscreenRenderTargetHeight);

	// Initialize the distance renderer.
	// The algorithm seems to break down if the target width is any smaller than 16 times the source width.
	// This is sufficient to reduce the memory requirement for the text message texture by a factor of 768, given the 
	// reduction in channels:
	//   * Memory required to store the offscreen DirectWrite source texture: 2048 * 2048 * 3 = 12,582,912 bytes
	//   * Memory required to store the offscreen distance field texture:      128 *  128 * 1 =     16,384 bytes
	constexpr unsigned int blurTargetWidth = 256;
	m_distanceFieldRenderer = std::make_unique<DistanceFieldRenderer>(m_deviceResources, blurTargetWidth, blurTargetWidth);

	// Use DirectWrite to create a high-resolution, antialiased image of vector-based text.
	RenderOffscreenTexture();
	///////////////////////////////////////////////////////////////////////////////////////
#endif
}

void SemanticPaintOnHololensMain::RenderOffscreenTexture()
{
	m_textRenderer->RenderTextOffscreen(L"Hello, Hologram!\n 2nd line");
}

void SemanticPaintOnHololensMain::UnregisterHolographicEventHandlers() {
	if (m_holographicSpace != nullptr) {
		// Clear previous event registrations.

		if (m_cameraAddedToken.Value != 0) {
			m_holographicSpace->CameraAdded -= m_cameraAddedToken;
			m_cameraAddedToken.Value = 0;
		}

		if (m_cameraRemovedToken.Value != 0) {
			m_holographicSpace->CameraRemoved -= m_cameraRemovedToken;
			m_cameraRemovedToken.Value = 0;
		}
	}

	if (m_locator != nullptr) {
		m_locator->PositionalTrackingDeactivating -= m_positionalTrackingDeactivatingToken;
	}

	if (m_surfaceObserver != nullptr) {
		m_surfaceObserver->ObservedSurfacesChanged -= m_surfacesChangedToken;
	}
}

SemanticPaintOnHololensMain::~SemanticPaintOnHololensMain() {
	// Deregister device notification.
	m_deviceResources->RegisterDeviceNotify(nullptr);

	UnregisterHolographicEventHandlers();
}

void SemanticPaintOnHololensMain::OnSurfacesChanged(
	SpatialSurfaceObserver^ sender,
	Object^ args) {
	IMapView<Guid, SpatialSurfaceInfo^>^ const& surfaceCollection = sender->GetObservedSurfaces();

	// Process surface adds and updates.
	for (const auto& pair : surfaceCollection) {
		auto id = pair->Key;
		auto surfaceInfo = pair->Value;

		//pipeline->run_mode_specific_section();

		// Choose whether to add, or update the surface.
		// In your app, you might choose to process added surfaces differently than updated
		// surfaces. For example, you might prioritize processing of added surfaces, and
		// defer processing of updates to existing surfaces.
		if (m_meshParser->HasSurface(id)) {
			if (m_meshParser->GetLastUpdateTime(id).UniversalTime < surfaceInfo->UpdateTime.UniversalTime && m_scanning) {
				// Update existing surface.
				m_meshParser->UpdateSurface(id, surfaceInfo);
			}
		}
		else {
			// New surface.
			m_meshParser->AddSurface(id, surfaceInfo);
		}
	}

	// Sometimes, a mesh will fall outside the area that is currently visible to
	// the surface observer. In this code sample, we "sleep" any meshes that are
	// not included in the surface collection to avoid rendering them.
	// The system can including them in the collection again later, in which case
	// they will no longer be hidden.
	m_meshParser->HideInactiveMeshes(surfaceCollection);
}

#ifdef NO_VOICE
#pragma optimize ( "", off )
void SemanticPaintOnHololensMain::novoice() {
	int mode = 0;
	switch (mode) {
	case 1:
		m_pipeline->set_mode(Pipeline::MODE_PREDICTION);
		break;
	case 2:
		m_pipeline->set_mode(Pipeline::MODE_TRAINING);
		break;
	case 3:
		m_pipeline->set_mode(Pipeline::MODE_PROPAGATION);
		break;
	case 4:
		m_pipeline->set_mode(Pipeline::MODE_NORMAL);
		break;
	case 5:
		m_scanning = !m_scanning;
	case 6:
		printTimingsFromPipeline();
	case 0:
		break;
	}

	int newLabel = 0;
	switch (newLabel){
	case 1:
		analyze_label_recognition(L"sofa");
		break;
	case 2:
		analyze_label_recognition(L"table");
		break;
	case 3:
		analyze_label_recognition(L"floor");
		break;
	case 4:
		analyze_label_recognition(L"chair");
		break;
	case 5:
		analyze_label_recognition(L"floor");
		break;
	}
}
#pragma optimize ( "", on )
#endif

// Updates the application state once per frame.
HolographicFrame^ SemanticPaintOnHololensMain::Update() {
#ifdef NO_VOICE
	novoice();
#else
	//Voice recognition input
	if (m_lastCommand != nullptr) {
		m_scanning = false;
		auto command = m_lastCommand;
		m_lastCommand = nullptr;

		int i = 0;
		for each (auto& iter in m_speechCommandList) {
			if (iter == command) {
				PrintWstringToDebugConsole(
					std::wstring(L"\nMode changed. New mode: ") +
					std::wstring(begin(command), end(command)) +
					L"\n");
				//m_labelAvailable = false;//Labelling not possible if mode changes
				m_pipeline->set_mode(m_speechCommandData[i]);

				break;
			}

			++i;
		}
		if (command == L"scan") {
			m_scanning = true;
		}
		if (command == L"get time")
			printTimingsFromPipeline();
	}
#endif
	process_labelling_input();



	// Before doing the timer update, there is some work to do per-frame
	// to maintain holographic rendering. First, we will get information
	// about the current frame.

	// The HolographicFrame has information that the app needs in order
	// to update and render the current frame. The app begins each new
	// frame by calling CreateNextFrame.
	HolographicFrame^ holographicFrame = m_holographicSpace->CreateNextFrame();

	// Get a prediction of where holographic cameras will be when this frame
	// is presented.
	HolographicFramePrediction^ prediction = holographicFrame->CurrentPrediction;

	// Back buffers can change from frame to frame. Validate each buffer, and recreate
	// resource views and depth buffers as needed.
	m_deviceResources->EnsureCameraResources(holographicFrame, prediction);

	// Next, we get a coordinate system from the attached frame of reference that is
	// associated with the current frame. Later, this coordinate system is used for
	// for creating the stereo view matrices when rendering the sample content.
	SpatialCoordinateSystem^ currentCoordinateSystem = m_referenceFrame->GetStationaryCoordinateSystemAtTimestamp(prediction->Timestamp);

	// Only create a surface observer when you need to - do not create a new one each frame.
	if (m_surfaceObserver == nullptr) {
		// Initialize the Surface Observer using a valid coordinate system.
		if (!m_spatialPerceptionAccessRequested) {
			// The spatial mapping API reads information about the user's environment. The user must
			// grant permission to the app to use this capability of the Windows Holographic device.
			auto initSurfaceObserverTask = create_task(SpatialSurfaceObserver::RequestAccessAsync());
			initSurfaceObserverTask.then([this, currentCoordinateSystem](Windows::Perception::Spatial::SpatialPerceptionAccessStatus status) {
				switch (status) {
				case SpatialPerceptionAccessStatus::Allowed:
					m_surfaceAccessAllowed = true;
					break;
				default:
					// Access was denied. This usually happens because your AppX manifest file does not declare the
					// spatialPerception capability.
					// For info on what else can cause this, see: http://msdn.microsoft.com/library/windows/apps/mt621422.aspx
					m_surfaceAccessAllowed = false;
					break;
				}
			});

			m_spatialPerceptionAccessRequested = true;
		}
	}

	if (m_surfaceAccessAllowed) {
		SpatialBoundingBox axisAlignedBoundingBox =
		{
			{  0.f,  0.f, 0.f },// center,
			{ 4.f, 4.f, 4.f },// Extents,
		};
		SpatialBoundingVolume^ bounds = SpatialBoundingVolume::FromBox(currentCoordinateSystem, axisAlignedBoundingBox);

		// If status is Allowed, we can create the surface observer.
		if (m_surfaceObserver == nullptr) {
			// First, we'll set up the surface observer to use our preferred data formats.
			// In this example, a "preferred" format is chosen that is compatible with our precompiled shader pipeline.
			m_surfaceMeshOptions = ref new SpatialSurfaceMeshOptions();
			IVectorView<DirectXPixelFormat>^ supportedVertexPositionFormats = m_surfaceMeshOptions->SupportedVertexPositionFormats;
			unsigned int formatIndex = 0;
			if (supportedVertexPositionFormats->IndexOf(DirectXPixelFormat::R16G16B16A16IntNormalized, &formatIndex)) {
				m_surfaceMeshOptions->VertexPositionFormat = DirectXPixelFormat::R16G16B16A16IntNormalized;
			}
			IVectorView<DirectXPixelFormat>^ supportedVertexNormalFormats = m_surfaceMeshOptions->SupportedVertexNormalFormats;
			if (supportedVertexNormalFormats->IndexOf(DirectXPixelFormat::R8G8B8A8IntNormalized, &formatIndex)) {
				m_surfaceMeshOptions->VertexNormalFormat = DirectXPixelFormat::R8G8B8A8IntNormalized;
			}

			// If you are using a very high detail setting with spatial mapping, it can be beneficial
			// to use a 32-bit unsigned integer format for indices instead of the default 16-bit. 
			// Uncomment the following code to enable 32-bit indices.
			//IVectorView<DirectXPixelFormat>^ supportedTriangleIndexFormats = m_surfaceMeshOptions->SupportedTriangleIndexFormats;
			//if (supportedTriangleIndexFormats->IndexOf(DirectXPixelFormat::R32UInt, &formatIndex))
			//{
			//    m_surfaceMeshOptions->TriangleIndexFormat = DirectXPixelFormat::R32UInt;
			//}

			// Create the observer.
			m_surfaceObserver = ref new SpatialSurfaceObserver();
			if (m_surfaceObserver) {
				m_surfaceObserver->SetBoundingVolume(bounds);

				// If the surface observer was successfully created, we can initialize our
				// collection by pulling the current data set.
				auto mapContainingSurfaceCollection = m_surfaceObserver->GetObservedSurfaces();
				for (auto const& pair : mapContainingSurfaceCollection) {
					// Store the ID and metadata for each surface.
					auto const& id = pair->Key;
					auto const& surfaceInfo = pair->Value;
					m_meshParser->AddSurface(id, surfaceInfo);
				}

				// We then subcribe to an event to receive up-to-date data.
				m_surfacesChangedToken = m_surfaceObserver->ObservedSurfacesChanged +=
					ref new TypedEventHandler<SpatialSurfaceObserver^, Platform::Object^>(
						bind(&SemanticPaintOnHololensMain::OnSurfacesChanged, this, _1, _2)
						);
			}
		}

		// Keep the surface observer positioned at the device's location.
		m_surfaceObserver->SetBoundingVolume(bounds);

		// Note that it is possible to set multiple bounding volumes. Pseudocode:
		//     m_surfaceObserver->SetBoundingVolumes(/* iterable collection of bounding volumes*/);
		//
		// It is also possible to use other bounding shapes - such as a view frustum. Pseudocode:
		//     SpatialBoundingVolume^ bounds = SpatialBoundingVolume::FromFrustum(coordinateSystem, viewFrustum);
		//     m_surfaceObserver->SetBoundingVolume(bounds);
	}

	m_timer.Tick([&]() {
		SpatialPointerPose^ pose = SpatialPointerPose::TryGetAtTimestamp(currentCoordinateSystem, prediction->Timestamp);
		m_meshParser->Update(m_timer, currentCoordinateSystem);
		m_meshParser->search_insightTriangle(pose);
#ifdef BOX_2D
		m_quadRenderer->UpdateHologramPosition(pose);
		m_quadRenderer->Update();
#endif
	});

	// The holographic frame will be used to get up-to-date view and projection matrices and
	// to present the swap chain.
	return holographicFrame;
}

// Renders the current frame to each holographic camera, according to the
// current application and spatial positioning state. Returns true if the
// frame was rendered to at least one camera.
bool SemanticPaintOnHololensMain::Render(
	HolographicFrame^ holographicFrame) {
	// Don't try to render anything before the first Update.
	if (m_timer.GetFrameCount() == 0) {
		return false;
	}

	// Lock the set of holographic camera resources, then draw to each camera
	// in this frame.
	return m_deviceResources->UseHolographicCameraResources<bool>(
		[this, holographicFrame](std::map<UINT32, std::unique_ptr<DX::CameraResources>>& cameraResourceMap) {
		// Up-to-date frame predictions enhance the effectiveness of image stablization and
		// allow more accurate positioning of holograms.
		holographicFrame->UpdateCurrentPrediction();
		HolographicFramePrediction^ prediction = holographicFrame->CurrentPrediction;
#ifdef BOX_2D
		// Off-screen drawing used by all cameras.
		// The distance function texture only needs to be created once, for a given text string.
		if (m_distanceFieldRenderer->GetRenderCount() == 0)
		{
			m_distanceFieldRenderer->RenderDistanceField(m_textRenderer->GetTexture());

			// The 2048x2048 texture will not be needed again, unless we get hit DeviceLost scenario.
			m_textRenderer->ReleaseDeviceDependentResources();
		}
#endif

		SpatialCoordinateSystem^ currentCoordinateSystem = m_referenceFrame->GetStationaryCoordinateSystemAtTimestamp(prediction->Timestamp);

		bool atLeastOneCameraRendered = false;
		for (auto cameraPose : prediction->CameraPoses) {
			// This represents the device-based resources for a HolographicCamera.
			DX::CameraResources* pCameraResources = cameraResourceMap[cameraPose->HolographicCamera->Id].get();

			// Get the device context.
			const auto context = m_deviceResources->GetD3DDeviceContext();
			const auto depthStencilView = pCameraResources->GetDepthStencilView();

			// Set render targets to the current holographic camera.
			ID3D11RenderTargetView *const targets[1] = { pCameraResources->GetBackBufferRenderTargetView() };
			context->OMSetRenderTargets(1, targets, depthStencilView);

			// Clear the back buffer and depth stencil view.
			context->ClearRenderTargetView(targets[0], DirectX::Colors::Transparent);
			context->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

			// The view and projection matrices for each holographic camera will change
			// every frame. This function refreshes the data in the constant buffer for
			// the holographic camera indicated by cameraPose.
			pCameraResources->UpdateViewProjectionBuffer(m_deviceResources, cameraPose, currentCoordinateSystem);

			// Attach the view/projection constant buffer for this camera to the graphics pipeline.
			bool cameraActive = pCameraResources->AttachViewProjectionBuffer(m_deviceResources);

			// Only render world-locked content when positional tracking is active.
			if (cameraActive) {
				m_meshParser->Render(pCameraResources->IsRenderingStereoscopic(), m_drawWireframe);
#ifdef BOX_2D
				m_quadRenderer->Render(m_distanceFieldRenderer->GetTexture());
#endif
			}
			atLeastOneCameraRendered = true;
		}

		return atLeastOneCameraRendered;
	});
}

void SemanticPaintOnHololensMain::analyze_label_recognition(Platform::String^ label) {
#ifndef NO_VOICE
		m_labelSpeechRecognizer->ContinuousRecognitionSession->PauseAsync();
#endif
		m_actualLabel = std::string(begin(label), end(label));
		OutputDebugStringW(L"\n");
		PrintWstringToDebugConsole(std::wstring(begin(m_actualLabel), end(m_actualLabel)));
		LabelManager_Ptr labelManager = m_pipeline->getLabelManager();
		if (labelManager->add_label(m_actualLabel)) {
			OutputDebugStringW(L"\nThis is a NEW label.\n");
		}
		else {
			OutputDebugStringW(L"\nLabel already in the system.\n");
		}
		m_labelAvailable = true;
#ifndef NO_VOICE
		m_ModeSpeechRecognizer->ContinuousRecognitionSession->Resume();//Any mode change will cause the labelling to stop
#endif
}

void SemanticPaintOnHololensMain::process_labelling_input() {
	if (m_labelAvailable) {
		// Check for new input state since the last frame.
		SpatialInteractionSourceState^ pointerState = m_spatialInputHandler->CheckForInput();

		if (pointerState != nullptr ) {
			auto pointerPose = pointerState->TryGetPointerPose(m_staticReferenceFrame->CoordinateSystem);
			if (pointerPose != nullptr) {
				// Get the gaze direction relative to the given coordinate system.
				const float3 headPosition = pointerPose->Head->Position;
				const float3 headDirection = pointerPose->Head->ForwardDirection;

				// When a Pressed gesture is detected, the label will be applied to the triangles in front of user
				LabelManager_CPtr labelManager = m_pipeline->getLabelManager();

				SpaintTriangleMesh::Label semanticLabel = labelManager->get_label(m_actualLabel); //Get the label
				const SpaintTriangleMesh::PackedLabel packedLabel(semanticLabel, SpaintTriangleMesh::LG_USER);
				m_meshParser->setLabelOnTriangle(packedLabel);
			}

		}
	}
}

void SemanticPaintOnHololensMain::SaveAppState() {
	// TODO load anchor point and labels
}

void SemanticPaintOnHololensMain::LoadAppState() {
	// TODO load anchor point and labels
}

// Notifies classes that use Direct3D device resources that the device resources
// need to be released before this method returns.
void SemanticPaintOnHololensMain::OnDeviceLost() {
	m_meshParser->ReleaseDeviceDependentResources();
#ifdef BOX_2D
	m_quadRenderer->ReleaseDeviceDependentResources();
	m_distanceFieldRenderer->ReleaseDeviceDependentResources();
	m_textRenderer->ReleaseDeviceDependentResources();
#endif
}

// Notifies classes that use Direct3D device resources that the device resources
// may now be recreated.
void SemanticPaintOnHololensMain::OnDeviceRestored() {
	m_meshParser->CreateDeviceDependentResources();
#ifdef BOX_2D
	m_quadRenderer->CreateDeviceDependentResources();
	m_textRenderer->CreateDeviceDependentResources();
	m_distanceFieldRenderer->CreateDeviceDependentResources();
	RenderOffscreenTexture();
#endif
}

void SemanticPaintOnHololensMain::OnPositionalTrackingDeactivating(
	SpatialLocator^ sender,
	SpatialLocatorPositionalTrackingDeactivatingEventArgs^ args) {
	// Without positional tracking, spatial meshes will not be locatable.
	args->Canceled = true;
}

void SemanticPaintOnHololensMain::OnCameraAdded(
	HolographicSpace^ sender,
	HolographicSpaceCameraAddedEventArgs^ args) {
	Deferral^ deferral = args->GetDeferral();
	HolographicCamera^ holographicCamera = args->Camera;
	create_task([this, deferral, holographicCamera]() {
		// Create device-based resources for the holographic camera and add it to the list of
		// cameras used for updates and rendering. Notes:
		//   * Since this function may be called at any time, the AddHolographicCamera function
		//     waits until it can get a lock on the set of holographic camera resources before
		//     adding the new camera. At 60 frames per second this wait should not take long.
		//   * A subsequent Update will take the back buffer from the RenderingParameters of this
		//     camera's CameraPose and use it to create the ID3D11RenderTargetView for this camera.
		//     Content can then be rendered for the HolographicCamera.
		m_deviceResources->AddHolographicCamera(holographicCamera);

		// Holographic frame predictions will not include any information about this camera until
		// the deferral is completed.
		deferral->Complete();
	});
}

void SemanticPaintOnHololensMain::OnCameraRemoved(
	HolographicSpace^ sender,
	HolographicSpaceCameraRemovedEventArgs^ args) {
	// Before letting this callback return, ensure that all references to the back buffer
	// are released.
	// Since this function may be called at any time, the RemoveHolographicCamera function
	// waits until it can get a lock on the set of holographic camera resources before
	// deallocating resources for this camera. At 60 frames per second this wait should
	// not take long.
	m_deviceResources->RemoveHolographicCamera(args->Camera);
}
