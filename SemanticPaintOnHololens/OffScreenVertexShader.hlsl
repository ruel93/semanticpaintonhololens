// A constant buffer that stores per-mesh data.
cbuffer ModelConstantBuffer : register(b0)
{
	float4x4      modelToWorld;
	min16float4x4 normalToWorld;
};

// A constant buffer that stores each set of view and projection matrices in column-major format.
cbuffer ViewProjectionConstantBuffer : register(b1)
{
	float4x4 viewProjection;
};

// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
	min16float3 pos     : POSITION;
	min16float4	color	: COLOR0;
};

struct VertexShaderOutput
{
	min16float4 screenPos   : SV_POSITION;
	min16float4	color       : COLOR0;
};

VertexShaderOutput main(VertexShaderInput input) 
{
	VertexShaderOutput output;
	min16float4 pos = min16float4(input.pos, 1.0f);
	// Transform the vertex position into world space.
	pos = (min16float4)mul(pos, modelToWorld);

	// Correct for perspective and project the vertex position onto the camera plane
	pos = (min16float4)mul(pos, viewProjection);
	output.screenPos = (min16float4)pos;

	// Pass a color.
	output.color = input.color;
	return output;
}