//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#pragma once

//
// Comment out this preprocessor definition to disable all of the
// sample content.
//

#include "Common\DeviceResources.h"
#include "Common\StepTimer.h"

#include "SPaint\Pipeline.h"

#include "Content\SpatialInputHandler.h"
#include "Content\RealtimeSurfaceMeshParser.h"

#include "Content\DDSTextureLoader.h"
#include "Content\DistanceFieldRenderer.h"
#include "Content\QuadRenderer.h"
#include "Content\TextRenderer.h"


#include <collection.h>

// Updates, renders, and presents holographic content using Direct3D.
namespace SemanticPaintOnHololens
{

	class SemanticPaintOnHololensMain : public DX::IDeviceNotify
	{
	public:
		SemanticPaintOnHololensMain(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		~SemanticPaintOnHololensMain();

		// Sets the holographic space. This is our closest analogue to setting a new window
		// for the app.
		void SetHolographicSpace(Windows::Graphics::Holographic::HolographicSpace^ holographicSpace);

		// Starts the holographic frame and updates the content.
		Windows::Graphics::Holographic::HolographicFrame^ Update();

		// Renders holograms, including world-locked content.
		bool Render(Windows::Graphics::Holographic::HolographicFrame^ holographicFrame);

		

		// Handle saving and loading of app state owned by AppMain.
		void SaveAppState();
		void LoadAppState();

		// IDeviceNotify
		virtual void OnDeviceLost();
		virtual void OnDeviceRestored();

		// Handle surface change events.
		void OnSurfacesChanged(Windows::Perception::Spatial::Surfaces::SpatialSurfaceObserver^ sender, Platform::Object^ args);

#ifdef NO_VOICE
		void novoice();
#endif



	private:
		void PrintWstringToDebugConsole(std::wstring string) {
			OutputDebugStringW(string.c_str());
		}

		void printTimingsFromPipeline();

		//Label map & Management
		std::string m_actualLabel;
		void analyze_label_recognition(Platform::String^ label);
		bool m_labelAvailable = false;
		bool m_labelRecogniserStarted = false;

		bool m_scanning = false;
		//Text 2D Box
		std::unique_ptr<HolographicTagAlong::QuadRenderer>								m_quadRenderer;
		std::unique_ptr<HolographicTagAlong::TextRenderer>								m_textRenderer;
		std::unique_ptr<HolographicTagAlong::DistanceFieldRenderer>						m_distanceFieldRenderer;
		void RenderOffscreenTexture();

		Concurrency::task<bool> m_label_task;

		void CreateSpeechConstraintForModeSelection();
		void CreateSpeechConstraintForLabelling();
		void OnModeSelectionResultGenerated(Windows::Media::SpeechRecognition::SpeechContinuousRecognitionSession^ sender, Windows::Media::SpeechRecognition::SpeechContinuousRecognitionResultGeneratedEventArgs^ args);
		void OnLabelResultGenerated(Windows::Media::SpeechRecognition::SpeechContinuousRecognitionSession ^ sender, Windows::Media::SpeechRecognition::SpeechContinuousRecognitionResultGeneratedEventArgs ^ args);
		// Asynchronously creates resources for new holographic cameras.
		void OnCameraAdded(
			Windows::Graphics::Holographic::HolographicSpace^ sender,
			Windows::Graphics::Holographic::HolographicSpaceCameraAddedEventArgs^ args);

		// Synchronously releases resources for holographic cameras that are no longer
		// attached to the system.
		void OnCameraRemoved(
			Windows::Graphics::Holographic::HolographicSpace^ sender,
			Windows::Graphics::Holographic::HolographicSpaceCameraRemovedEventArgs^ args);

		// Used to prevent the device from deactivating positional tracking, which is 
		// necessary to continue to receive spatial mapping data.
		void OnPositionalTrackingDeactivating(
			Windows::Perception::Spatial::SpatialLocator^ sender,
			Windows::Perception::Spatial::SpatialLocatorPositionalTrackingDeactivatingEventArgs^ args);

		// Clears event registration state. Used when changing to a new HolographicSpace
		// and when tearing down AppMain.
		void UnregisterHolographicEventHandlers();

		//SPaint Application functions
		void process_labelling_input();

		// Listens for the Pressed spatial input event.
		std::shared_ptr<SpatialInputHandler>                                m_spatialInputHandler;

		// A data handler for surface meshes.
		std::unique_ptr<SemanticPaintOnHololensParser::RealtimeSurfaceMeshParser> m_meshParser;

		// Cached pointer to device resources.
		std::shared_ptr<DX::DeviceResources>                                m_deviceResources;

		std::shared_ptr<Pipeline>											m_pipeline;

		// Render loop timer.
		DX::StepTimer                                                       m_timer;

		// Represents the holographic space around the user.
		Windows::Graphics::Holographic::HolographicSpace^                   m_holographicSpace;

		// SpatialLocator that is attached to the primary camera.
		Windows::Perception::Spatial::SpatialLocator^                       m_locator;

		// A reference frame attached to the holographic camera.
		Windows::Perception::Spatial::SpatialLocatorAttachedFrameOfReference^	m_referenceFrame;
		Windows::Perception::Spatial::SpatialStationaryFrameOfReference^		m_staticReferenceFrame;

		Windows::Globalization::Language^									m_language;
		Windows::Media::SpeechRecognition::SpeechRecognizer^				m_ModeSpeechRecognizer;
		Windows::Media::SpeechRecognition::SpeechRecognizer^				m_labelSpeechRecognizer;
		Platform::Collections::Vector<Platform::String^>^					m_speechCommandList;
		Platform::Collections::Vector<Platform::String^>^					m_LabelDictionaryList;
		std::vector<SemanticPaintOnHololens::Pipeline::Mode>				m_speechCommandData;
		Platform::String^													m_lastCommand;

		// Event registration tokens.
		Windows::Foundation::EventRegistrationToken                         m_cameraAddedToken;
		Windows::Foundation::EventRegistrationToken                         m_cameraRemovedToken;
		Windows::Foundation::EventRegistrationToken                         m_positionalTrackingDeactivatingToken;
		Windows::Foundation::EventRegistrationToken                         m_surfacesChangedToken;

		// Indicates whether access to spatial mapping data has been granted.
		bool                                                                m_surfaceAccessAllowed = false;

		// Indicates whether the surface observer initialization process was started.
		bool                                                                m_spatialPerceptionAccessRequested = false;

		// Obtains spatial mapping data from the device in real time.
		Windows::Perception::Spatial::Surfaces::SpatialSurfaceObserver^     m_surfaceObserver;
		Windows::Perception::Spatial::Surfaces::SpatialSurfaceMeshOptions^  m_surfaceMeshOptions;

		// Determines the rendering mode.
		bool                                                                m_drawWireframe = false;
		bool																m_waitingForLabel = false;
	};
}
