#include <pch.h>
#include "TriangleMarker.h"
#include "TriangleMarker_Settings.h"
#include "Content\SurfaceMesh.h"

using namespace SemanticPaintOnHololensParser;

namespace SemanticPaintOnHololens {
	void TriangleMarker::clear_labels(SpaintTriangleMesh *triangles, int triangleCount, ClearingSettings settings) const {
#pragma omp parallel for
		for (int i = 0; i < triangleCount; ++i) {
			clear_label(triangles[i], settings);
		}
	}

	void TriangleMarker::mark_triangles(std::vector<size_t> triangleIndexes, SpaintTriangleMesh::PackedLabel label, SurfaceMesh& mesh, MarkingMode mode) const{
		auto& triangles = mesh.getTriangleVector();
#pragma omp parallel for
		for (uint16 i = 0; i < triangleIndexes.size(); ++i) {
			mark_triangle(label, triangles[triangleIndexes[i]], mode);
		}
	}

	void TriangleMarker::mark_triangles(std::vector<size_t>& triangleIndexes, std::vector<SpaintTriangleMesh::PackedLabel>& triangleLabels, SemanticPaintOnHololensParser::SurfaceMesh & mesh, MarkingMode mode) const {

		auto& triangles = mesh.getTriangleVector();
#pragma omp parallel for
		for (uint16 i = 0; i < triangleIndexes.size(); ++i) {
			mark_triangle(triangleLabels[i], triangles[triangleIndexes[i]], mode);
		}
	}

}