#pragma once
#include "pch.h"
#include "SPaint\MemoryBlock.h"
#include "SPaint\SpaintTriangleMesh.h"
#include "TriangleMarker.h"
#include "TriangleMarker_Settings.h"

class Interactor{
	//#################### TYPEDEFS ####################
private:
	typedef std::shared_ptr<ORUtils::MemoryBlock<SemanticPaintOnHololens::SpaintTriangleMesh::PackedLabel> > PackedLabels_Ptr;
	typedef std::shared_ptr<const ORUtils::MemoryBlock<SemanticPaintOnHololens::SpaintTriangleMesh::PackedLabel> > PackedLabels_CPtr;
	typedef std::shared_ptr<const SemanticPaintOnHololens::TriangleMarker> TriangleMarker_CPtr;

public:

	//#################### PRIVATE VARIABLES ####################
private:

	/** The semantic label to use for manually labelling the scene. */
	SemanticPaintOnHololens::SpaintTriangleMesh::Label m_semanticLabel;
	
	/** The voxel marker (used to apply semantic labels to voxels in the scene). */
	TriangleMarker_CPtr m_triangleMarker;

	//#################### CONSTRUCTORS ####################
public:
	/**
	* \brief Constructs an interactor that can be used to interact with the mesh
	*/
	explicit Interactor() = default;
	
	//#################### PUBLIC MEMBER FUNCTIONS ####################
public:
	/**
	* \brief Marks a selection of voxels in the scene with the specified semantic label.
	*
	* \param selection The selection of voxels.
	* \param label     The semantic label with which to mark the voxels.
	* \param oldLabels An optional memory block into which to store the old semantic labels of the voxels being marked.
	* \param mode      The marking mode.
	*/
	inline void mark_triangles(std::vector<size_t> triangleIndexes, std::vector<SemanticPaintOnHololens::SpaintTriangleMesh::PackedLabel>& triangleLabels, SemanticPaintOnHololensParser::SurfaceMesh& mesh, SemanticPaintOnHololens::MarkingMode mode = SemanticPaintOnHololens::NORMAL_MARKING){
		m_triangleMarker->mark_triangles(triangleIndexes, triangleLabels, mesh, mode);
	}

	/**
	* \brief Marks a selection of voxels in the scene with the specified semantic labels.
	*
	* \param selection The selection of voxels.
	* \param labels    The semantic labels with which to mark the voxels (one per voxel).
	* \param mode      The marking mode.
	*/
	inline void mark_triangles(std::vector<size_t> triangleIndexes, SemanticPaintOnHololens::SpaintTriangleMesh::PackedLabel label, SemanticPaintOnHololensParser::SurfaceMesh& mesh, SemanticPaintOnHololens::MarkingMode mode = SemanticPaintOnHololens::NORMAL_MARKING){
		m_triangleMarker->mark_triangles(triangleIndexes, label, mesh, mode);
	}
};

typedef std::shared_ptr<Interactor> Interactor_Ptr;
typedef std::shared_ptr<const Interactor> Interactor_CPtr;