#pragma once
#include "SPaint\MemoryBlock.h"
#include "pch.h"
#include <memory>

namespace SemanticPaintOnHololens {

	/**
	* \brief An instance of this class can be used to make memory blocks.
	*/
	class MemoryBlockFactory {
		//#################### PRIVATE VARIABLES ####################
	private:

		//#################### SINGLETON IMPLEMENTATION ####################
	private:
		/**
		* \brief Constructs a memory block factory.
		*/
		MemoryBlockFactory() {}

	public:
		/**
		* \brief Gets the singleton instance.
		*
		* \return  The singleton instance.
		*/
		//static MemoryBlockFactory& instance(); //NOT Compiling... external reference missing

		static MemoryBlockFactory& instance() { 
			static MemoryBlockFactory instance;
			return instance;
		}

		//Delete copy and assign methods
		MemoryBlockFactory(MemoryBlockFactory const&) = delete;
		void operator=(MemoryBlockFactory const&) = delete;

		//#################### PUBLIC MEMBER FUNCTIONS ####################
	public:
		/**
		* \brief Makes a memory block of the specified type and size.
		*
		* \param dataSize  The size of the memory block to make.
		* \return          The memory block.
		*/
		template <typename T>
		std::shared_ptr<ORUtils::MemoryBlock<T> > make_block(size_t dataSize) const {
			bool allocateGPU = false;
			return std::shared_ptr<ORUtils::MemoryBlock<T> >(new ORUtils::MemoryBlock<T>(dataSize, true, allocateGPU));
		}
	};

}